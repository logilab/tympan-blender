import bpy
import bmesh
from contextlib import contextmanager
from mathutils import (
    Matrix,
    Vector,
)
import numpy as np


LOWER_CUTOFF_FREQUENCIES = (
    16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500,
    630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000,
    10000, 12500, 16000
)


DEFAULT_SPECTRUM_BAND_AMPLITUDE = -200


def blty_idname(*parts):
    """Return a fully qualified bl_idname using `blty` namespace."""
    return '.'.join(('blty', ) + parts)


def initialise_spectrum_bands(spectrum_bands_property, amplitudes):
    spectrum_size = len(amplitudes)
    if spectrum_size == 31:
        frequencies = LOWER_CUTOFF_FREQUENCIES
    elif spectrum_size == 9:
        frequencies = LOWER_CUTOFF_FREQUENCIES[3:29:3]
    spectrum_bands_property.clear()
    for frequency, amplitude in zip(frequencies, amplitudes):
        band = spectrum_bands_property.add()
        band.frequency = frequency
        band.amplitude = amplitude


def initialise_spectrum(spectrum_property, values=None):
    if values is None:
        values = [DEFAULT_SPECTRUM_BAND_AMPLITUDE] * 31
    spectrum_len = len(tuple(values))
    assert spectrum_len == 31, ValueError(
        ('"values" argument must have 31 elements.'
         'Actual length: {}'.format(spectrum_len))
    )
    initialise_spectrum_bands(spectrum_property.bands, values)


def sum_db(amplitudes):
    """Sum of a list of spectra amplitudes"""
    cumulated_power_ratio = sum(10**(np.array(amplitudes)/10.0))
    return 10 * np.log10(cumulated_power_ratio)


def copy_spectrum_property(dest_spectrum, spectrum):
    dest_spectrum.bands.clear()
    dest_spectrum.scale = spectrum.scale
    for band in spectrum.bands:
        dest_spectrum.bands.add()
        dest_spectrum.bands[-1].amplitude = band.amplitude
        dest_spectrum.bands[-1].frequency = band.frequency


def spectrum_amplitudes(spectrum_property):
    return [band.amplitude for band in spectrum_property.bands]


def create_default_acoustic_material(material_name='Material'):
    material = bpy.data.materials.new(name=material_name)
    acoustic_props = material.acoustic_properties
    acoustic_props.name = material.name
    default_amplitudes = [0.2] * 31
    initialise_spectrum(acoustic_props.spectrum, values=default_amplitudes)
    return material


def origin_to_bottom(obj):
    '''move origin of the object at its bottom'''
    if len(obj.data.vertices) < 1:
        return
    min_z = min([(obj.matrix_world @ v.co).z for v in obj.data.vertices])
    bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS')
    origin = Vector((obj.location.x, obj.location.y, min_z))
    rotation_matrix = obj.rotation_euler.to_matrix().to_4x4()
    rotation_matrix.invert()
    for v in obj.data.vertices:
        v.co = (obj.matrix_world @ v.co) - origin
    obj.matrix_world.translation = origin
    obj.matrix_world = obj.matrix_world @ rotation_matrix


def redraw_3d_view():
    '''Otherwise the 3D view and panels will not be redrawn automatically'''
    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)


@contextmanager
def bmesh_from_mesh(mesh):
    if mesh.is_editmode:
        bm = bmesh.from_edit_mesh(mesh).copy()
    else:
        bm = bmesh.new()
        bm.from_mesh(mesh)
    yield bm
    bm.free()


@contextmanager
def bmesh_from_mesh_for_update(mesh):
    if mesh.is_editmode:
        bm = bmesh.from_edit_mesh(mesh)
        yield bm
        bmesh.update_edit_mesh(mesh)
    else:
        bm = bmesh.new()
        bm.from_mesh(mesh)
        yield bm
        bm.to_mesh(mesh)
        bm.free()


@contextmanager
def object_mode():
    previous_mode = bpy.context.object.mode
    bpy.ops.object.mode_set(mode='OBJECT')
    yield
    bpy.ops.object.mode_set(mode=previous_mode)


def create_vertex_color_node(node_tree, attr_name):
    nodes = node_tree.nodes
    v_color_node = nodes.new("ShaderNodeAttribute")
    v_color_node.attribute_name = attr_name
    return v_color_node.outputs[0]


def create_image_texture_node(name, node_tree, uv_layer):
    nodes = node_tree.nodes
    links = node_tree.links
    uv_map_node = nodes.new("ShaderNodeUVMap")
    uv_map_node.uv_map = uv_layer
    tex_image_node = nodes.new("ShaderNodeTexImage")
    tex_image_node.image = None
    tex_image_node.name = name
    tex_image_node.extension = "CLIP"
    links.new(uv_map_node.outputs[0], tex_image_node.inputs[0])
    return tex_image_node.outputs


def create_height_gradient_node(node_tree, node_name_prefix):
    nodes = node_tree.nodes
    links = node_tree.links

    geometry_node = nodes.new("ShaderNodeNewGeometry")
    sep_xyz = nodes.new("ShaderNodeSeparateXYZ")
    links.new(sep_xyz.inputs[0], geometry_node.outputs[0])

    map_range = nodes.new("ShaderNodeMapRange")
    map_range.name = f"{node_name_prefix}_range"
    map_range.inputs[1].default_value = 0  # From Min
    map_range.inputs[2].default_value = 1000  # From Max
    map_range.inputs[3].default_value = 0  # To Min
    map_range.inputs[4].default_value = 1  # To Max
    links.new(map_range.inputs[0], sep_xyz.outputs[2])

    val_to_rgb = nodes.new("ShaderNodeValToRGB")
    val_to_rgb.name = f"{node_name_prefix}_val_to_rgb"
    color_ramp = val_to_rgb.color_ramp
    # color_ramp.interpolation = "CARDINAL"
    colors = color_ramp.elements
    colors[0].color = (0.02, 0.1, 0.02, 1)
    colors[1].color = (0.9, 0.15, 0.20, 1)
    colors.new(0.25).color = (0.4, 0.8, 0.2, 1)
    colors.new(0.50).color = (1, 0.8, 0.3, 1)
    colors.new(0.75).color = (0.8, 0.4, 0.2, 1)
    links.new(val_to_rgb.inputs[0], map_range.outputs[0])

    return val_to_rgb.outputs[0]


def remove_texture(target_object, image_node_name):
    material = target_object.material_slots[0].material
    material.node_tree.nodes[image_node_name].image = None


def use_image_empty_as_texture(target_object, image_node_name, image_empty):
    material = target_object.material_slots[0].material
    material.node_tree.nodes[image_node_name].image = image_empty.data


def _texture_from_empty_matrix(image_empty):
    img_size = image_empty.data.size
    # TODO handle data properties (image size and offset)
    translation = Matrix.Translation((0.5, 0.5, 0))
    # In texture coordinates, the coordinates are in the [0, 1] range.
    # Setting the empty_display_size of the image object to 2 scales the image
    # so that the image fit in a 2x2 square. Depending on which side is the
    # longest, we have to map the other one to the range [0,1].
    if img_size[0] > img_size[1]:
        ratio = img_size[0] / img_size[1]
        scale = Matrix.Scale(ratio, 4, (0, 1, 0))
    else:
        ratio = img_size[1] / img_size[0]
        scale = Matrix.Scale(ratio, 4, (1, 0, 0))

    return translation @ scale


def project_image_empty_on_object(target_object, image_node_name, layer_idx,
                                  image_empty):
    use_image_empty_as_texture(target_object, image_node_name, image_empty)
    world_from_obj = target_object.matrix_world
    empty_from_world = image_empty.matrix_world.copy().inverted()
    texture_from_empty = _texture_from_empty_matrix(image_empty)
    texture_from_obj = texture_from_empty @ empty_from_world @ world_from_obj
    with bmesh_from_mesh_for_update(target_object.data) as bm:
        bm.faces.ensure_lookup_table()
        uv_layer = bm.loops.layers.uv[layer_idx]
        for face in bm.faces:
            for loop in face.loops:
                loop[uv_layer].uv = (texture_from_obj @ loop.vert.co).xy
