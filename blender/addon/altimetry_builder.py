from math import isclose
import numpy as np
import shapely
from shapely.geometry import Polygon
import bmesh
from bpy_extras import object_utils
import bpy.types

from tympan.altimetry import (
    AltimetryMesh,
    datamodel,
    builder,
)

from . import (
    core,
    element_types as et,
    infrastructure,
    tympan_utils as tu,
)
from blty.shader_node_layers_wrapper import ShaderNodeLayersWrapper


def altitude_from_vertices(vertices, feature_id):
    altitudes = vertices[:, -1]
    altitude = altitudes[0]
    if not np.allclose(altitudes, altitude, atol=1e-4):
        raise ValueError(
            '{0} vertices have inconsistent altitudes {1}'.format(
                feature_id, altitudes))
    return altitude


def get_counter_clockwise_orientation(point_list):
    """Use shapely to ensure polygon points are in a counterclockwise order"""
    point_list = shapely.geometry.polygon.orient(Polygon(point_list)).exterior
    return list(point_list.coords)[:-1]


def get_infra_contours(infra_obj):
    """Return the coordinates of ground base faces of an infra"""
    ground_contours = []
    points_by_faces = {}
    x_scale, y_scale, _ = infra_obj.scale
    x_loc, y_loc, _ = infra_obj.location
    # points by faces
    for face_index, polygon in enumerate(infra_obj.data.polygons):
        verts_in_face = polygon.vertices[:]
        # keep only horizontal faces (same altitude)
        points_alti = [infra_obj.data.vertices[vert].co[-1]
                       for vert in verts_in_face]
        if np.allclose(points_alti, points_alti[0], atol=0.01):
            for vert in verts_in_face:
                vector = infra_obj.data.vertices[vert].co
                mean_alti = np.mean(points_alti)
                points_by_faces.setdefault(mean_alti, []).append(vector)
    # keep only base faces (lowest altitude)
    min_alti = min(points_by_faces)
    for alti, points in points_by_faces.items():
        if isclose(alti, min_alti, rel_tol=0.01):
            points_2d = core.apply_object_transforms(points, infra_obj)[:, :2]
            coords = get_counter_clockwise_orientation(points_2d)
            ground_contours.append(coords)
    return ground_contours


def _get_landtake_points(context):
    width = context.scene.landtake_xsize
    height = context.scene.landtake_ysize
    return np.array([
        (-width/2, -height/2),
        (-width/2, height/2),
        (width/2, height/2),
        (width/2, -height/2),
    ])


def _add_landtake(landtake_points, altitude, altimetry_site):
    landtake_id = 'default-landtake'
    altimetry_site.add_child(
        datamodel.SiteLandtake(landtake_points, altitude=altitude,
                               id=landtake_id + '-level_curve')
    )


class LevelCurveConsistencyError(Exception):
    pass


class UnconnectedVertexError(LevelCurveConsistencyError):

    def __init__(self):
        super().__init__("Found an unconnected vertex in level curve")


class TooManyEdgesError(LevelCurveConsistencyError):

    def __init__(self):
        msg = "Found a vertex connected to 3 or more edges in level curve"
        super().__init__(msg)


class OpenPathError(LevelCurveConsistencyError):

    def __init__(self):
        msg = "Found a vertex connected to less that 2 edges"
        super().__init__(msg)


def _linked_verts(vert):
    return [v for e in vert.link_edges for v in e.verts if v != vert]


def _walk_vertices(first_vert):
    path = [first_vert]
    previous_vert = first_vert
    linked = _linked_verts(first_vert)
    current_vert = linked[0]
    path.append(current_vert)
    while True:
        linked = _linked_verts(current_vert)
        if len(linked) == 2:
            next_vert = linked[0] if linked[1] == previous_vert else linked[1]
            previous_vert, current_vert = current_vert, next_vert
            path.append(current_vert)
            if current_vert == first_vert:
                return path
        elif len(linked) == 1:
            return path


def _check_level_curve_consistency(bm):
    """Check that no vertex is unconnected or connected to more than two
    vertices"""
    connections_count = [len(v.link_edges) for v in bm.verts]
    if any([cc < 1 for cc in connections_count]):
        raise UnconnectedVertexError()
    if any([cc > 2 for cc in connections_count]):
        raise TooManyEdgesError()


def _check_water_body_consistency(bm):
    _check_level_curve_consistency(bm)
    connections_count = [len(v.link_edges) for v in bm.verts]
    if any([cc < 2 for cc in connections_count]):
        raise OpenPathError()


def _paths_from_bmesh(bm, matrix_world):
    consumed_verts = set()
    # Start by yielding un-closed paths, which are paths containing vertices
    # with only one edge. Unconsummed vertices will necessarily be part of a
    # closed path.
    verts = sorted(bm.verts, key=lambda v: len(v.link_edges))
    for vert in verts:
        if vert not in consumed_verts:
            path_rel = _walk_vertices(vert)
            consumed_verts |= set(path_rel)
            yield np.array([matrix_world @ v.co for v in path_rel])


def _paths_from_object(obj, handle_error_cb):
    with tu.bmesh_from_mesh(obj.data) as bm:
        paths = _paths_from_bmesh(bm, obj.matrix_world)
        features_desc = []
        for idx, path in enumerate(paths):
            feature_id = f"{obj.name}-{idx}"
            try:
                altitude = altitude_from_vertices(path, feature_id)
            except ValueError as exc:
                handle_error_cb(str(exc))
                continue
            points2d = path[:, :2]
            features_desc.append((feature_id, points2d, altitude))
    return features_desc


def _add_level_curves(context, altimetry_site, handle_error_cb):
    for obj in context.scene.objects:
        if (isinstance(obj.data, bpy.types.Mesh)
                and et.is_level_curve(obj)):
            try:
                with tu.bmesh_from_mesh(obj.data) as bm:
                    _check_level_curve_consistency(bm)
                features_desc = _paths_from_object(obj, handle_error_cb)
            except LevelCurveConsistencyError as exc:
                handle_error_cb(f'Error on {obj.name}: {exc}')
            else:
                for feature_id, coords, altitude in features_desc:
                    lcurve = datamodel.LevelCurve(
                        coords=coords, altitude=altitude, id=feature_id)
                    assert lcurve.shape.is_valid, obj
                    altimetry_site.add_child(lcurve)


def _add_infrastructures(context, altimetry_site):
    for obj in infrastructure.find_all_infrastructures(context):
        contours_coords = get_infra_contours(obj)
        infra_landtake = datamodel.InfrastructureLandtake(*contours_coords,
                                                          id=obj.name)
        assert infra_landtake.shape.is_valid, obj
        altimetry_site.add_child(infra_landtake)


def find_ground_sets(context):
    for obj in context.scene.objects:
        if et.is_ground_set(obj):
            yield obj


def _ground_id_to_tympan_material_uid(ground_set, ground):
    return f'MATERIAL-{ground_set.name}-{ground.id}'


def _ground_set_to_materials(ground_set):
    from tympan.altimetry import datamodel
    return {
        ground.id: datamodel.GroundMaterial(
            _ground_id_to_tympan_material_uid(ground_set, ground),
            resistivity=ground.resistivity
            )
        for ground in ground_set.tympan_grounds
    }


def _tympan_material_uid_to_color(context):
    mapping = {
        "Water": (0, 0.4, 1, 1)
    }
    for ground_set in find_ground_sets(context):
        for ground in ground_set.tympan_grounds:
            uid = _ground_id_to_tympan_material_uid(ground_set, ground)
            mapping[uid] = ground.material.diffuse_color
    return mapping


def _add_material_areas(context, altimetry_site):
    for obj in find_ground_sets(context):
        materials = _ground_set_to_materials(obj)
        with tu.bmesh_from_mesh(obj.data) as bm:
            ground_layer = bm.faces.layers.int.get('ground')
            for face_idx, face in enumerate(bm.faces):
                ground_id = face[ground_layer]
                material = materials[ground_id]
                mat_area_id = f'{obj.name}-{face_idx}'
                mat_area = datamodel.MaterialArea(
                    [(obj.matrix_world @ v.co).xy for v in face.verts],
                    material, id=mat_area_id)
                altimetry_site.add_child(mat_area)


def find_water_bodies(context):
    for obj in context.scene.objects:
        if et.is_water_body(obj):
            yield obj


def _add_water_bodies(context, altimetry_site, handle_error_cb):
    for obj in find_water_bodies(context):
        try:
            with tu.bmesh_from_mesh(obj.data) as bm:
                _check_water_body_consistency(bm)
            features_desc = _paths_from_object(obj, handle_error_cb)
        except LevelCurveConsistencyError as exc:
            handle_error_cb(f'Error on {obj.name}: {exc}')
        else:
            for feature_id, coords, altitude in features_desc:
                lcurve = datamodel.WaterBody(
                    coords=coords, altitude=altitude, id=feature_id)
                assert lcurve.shape.is_valid, obj
                altimetry_site.add_child(lcurve)


def _create_altimetry_material():
    material = bpy.data.materials.new("Altimetry")
    material.use_nodes = True
    node_tree = material.node_tree
    nodes = node_tree.nodes
    links = node_tree.links

    base_color = nodes.new("ShaderNodeRGB")
    base_color.outputs[0].default_value = (0.1, 0.1, 0.1, 1)

    mat_wrapper = ShaderNodeLayersWrapper(material, base_color.outputs[0])

    height_color_out = tu.create_height_gradient_node(node_tree, "height_map")
    mat_wrapper.add_layer("height", height_color_out, initial_opacity=0)

    # Add 4 image layers allowing to display up to 4 projected reference images
    for i in range(4):
        image_node_name = f"Image-{i}"
        img_color_out, img_alpha_out = tu.create_image_texture_node(
            image_node_name, node_tree, f"uvmap-{i}"
        )
        mat_wrapper.add_layer(f"image-{i}", img_color_out, img_alpha_out,
                              initial_opacity=1)

    ground_color_out = tu.create_vertex_color_node(node_tree, "grounds")
    mat_wrapper.add_layer("grounds", ground_color_out)

    diffuse_node = nodes.new("ShaderNodeBsdfDiffuse")
    links.new(diffuse_node.inputs[0], mat_wrapper.color_output)

    material_output = material.node_tree.nodes.get('Material Output')
    links.new(material_output.inputs[0], diffuse_node.outputs[0])
    return material


def _prepare_altimetry_mesh(context):
    """Create or retrieve the altimetry mesh"""
    for obj in bpy.data.objects:
        if et.is_altimetry_mesh(obj):
            break
    else:
        me = bpy.data.meshes.new('altimetry')
        obj = object_utils.object_data_add(context, me, operator=None)
        et.set_object_type(obj, 'ALTIMETRY')
        et.set_object_subtype(obj, 'ALTIMETRY MESH')
        material = _create_altimetry_material()
        me.materials.append(material)
    obj.location = (0, 0, 0)
    obj.lock_location = (True, True, True)
    obj.lock_rotation = (True, True, True)
    obj.lock_scale = (True, True, True)
    context.view_layer.update()
    return obj


def _update_altimetry_mesh(context, ty_alti, area_id_to_color):
    vertices_co, faces = ty_alti.mesh.as_arrays()
    bm = bmesh.new()
    bm_verts = [bm.verts.new(v) for v in vertices_co]
    obj = _prepare_altimetry_mesh(context)
    grounds_layer = bm.loops.layers.color.new("grounds")
    material_by_face = ty_alti.material_by_face
    faces_material = ty_alti.mesh.faces_material(material_by_face)
    ground_colors = _tympan_material_uid_to_color(context)
    for face_idx, face in enumerate(faces):
        bmface = bm.faces.new([bm_verts[i] for i in face])
        bm.faces.ensure_lookup_table()
        material_id = faces_material[face_idx].id
        for loop in bmface.loops:
            loop[grounds_layer] = ground_colors.get(
                material_id,
                (0.2, 0.2, 0.2, 1)
            )

    bm.to_mesh(obj.data)
    obj.data.update()
    for idx in range(4):
        obj.data.uv_layers.new(name=f"uvmap-{idx}")

    infrastructure.apply_infrastructures_elevation(context, obj.data)
    core.apply_sources_receivers_elevation(context, obj)
    context = bpy.context.copy()
    context["object"] = obj
    bpy.ops.blty.update_altimetry_projection(context)


def build_altimetry(context, handle_error_cb):
    """Return an `AltimetryMesh` from data found Blender `context`."""
    landtake_points = _get_landtake_points(context)
    altimetry_site = datamodel.SiteNode(coords=landtake_points, id="site")
    if context.scene.landtake_used_as_level_curve:
        landtake_height = context.scene.landtake_height
        _add_landtake(landtake_points, landtake_height, altimetry_site)
    _add_level_curves(context, altimetry_site, handle_error_cb)
    _add_water_bodies(context, altimetry_site, handle_error_cb)
    _add_infrastructures(context, altimetry_site)
    area_id_to_color = _add_material_areas(context, altimetry_site)
    try:
        merged_site, mesh, feature_by_face = builder.build_altimetry(
            altimetry_site)
    except datamodel.InconsistentGeometricModel as exc:
        handle_error_cb(str(exc))
        return None
    altimetry_mesh = AltimetryMesh(merged_site, mesh, feature_by_face)
    _update_altimetry_mesh(context, altimetry_mesh, area_id_to_color)
    return altimetry_mesh
