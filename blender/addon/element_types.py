import bpy

from . import tympan_utils as tu


_TYMPAN_TYPES = {
    'SOURCE': 'FORCE_FORCE',
    'RECEIVER': 'FORCE_TURBULENCE',
    'INFRASTRUCTURE': 'MOD_BUILD',
    'ALTIMETRY': 'WORLD_DATA',
    'REFERENCE_IMAGE': 'FILE_IMAGE',
    'UNDEFINED': 'BLENDER',
}

_TYMPAN_SUBTYPES = {
    'INFRASTRUCTURE': {
        'BUILDING': 'MOD_BUILD',
        'MACHINE': 'SCRIPTWIN',
        'SCREEN': 'MESH_PLANE',
    },
    'RECEIVER': {
        'POINT': 'LAYER_ACTIVE',
    },
    'SOURCE': {
        'POINT': 'FORCE_FORCE',
    },
    'ALTIMETRY': {
        'LEVEL CURVE': 'OUTLINER_DATA_CURVE',
        'ALTIMETRY MESH': 'RNDCURVE',
        'GROUND SET': 'DECORATE_KEYFRAME',
        'WATER BODY': 'MOD_OCEAN',
    },
    'REFERENCE_IMAGE': {
        'TOPOGRAPHIC_MAP': 'RNDCURVE',
        'MASS_PLAN': 'IMAGE_ZDEPTH',
    },
    'UNDEFINED': {
        'UNDEFINED': 'BLENDER',
    },
}

_OBJECT_TYPE_KEY = 'tympan_type'
_OBJECT_SUBTYPE_KEY = 'tympan_subtype'


def set_object_type(obj, new_type):
    setattr(obj, _OBJECT_TYPE_KEY, new_type)


def get_object_type(obj):
    return getattr(obj, _OBJECT_TYPE_KEY)


def set_object_subtype(obj, new_subtype):
    setattr(obj, _OBJECT_SUBTYPE_KEY, new_subtype)


def get_object_subtype(obj):
    return getattr(obj, _OBJECT_SUBTYPE_KEY)


def types():
    return tuple(sorted(_TYMPAN_TYPES))


def subtypes(tympan_type):
    return tuple(sorted(_TYMPAN_SUBTYPES.get(tympan_type, {})))


def type_icon(tympan_type, subtype=None):
    if subtype is None:
        return _TYMPAN_TYPES[tympan_type]
    return _TYMPAN_SUBTYPES[tympan_type][subtype]


def object_icon(obj):
    tympan_type = get_object_type(obj)
    tympan_subtype = get_object_subtype(obj)
    if tympan_subtype not in _TYMPAN_SUBTYPES[tympan_type]:
        tympan_subtype = None
    return type_icon(tympan_type, tympan_subtype)


def is_infrastructure(obj):
    return get_object_type(obj) == 'INFRASTRUCTURE'


def is_building(obj):
    return is_infrastructure(obj) and get_object_subtype(obj) == 'BUILDING'


def is_machine(obj):
    return is_infrastructure(obj) and get_object_subtype(obj) == 'MACHINE'


def is_screen(obj):
    return is_infrastructure(obj) and get_object_subtype(obj) == 'SCREEN'


def is_internal_source(obj):
    return getattr(obj, 'parent_building')


def is_internal_point_source(obj):
    return is_source(obj) and is_internal_source(obj)


def is_internal_machine(obj):
    return is_machine(obj) and is_internal_source(obj)


def is_external_source(obj):
    return is_source(obj) and not is_internal_source(obj)


def is_external_machine(obj):
    return is_machine(obj) and not is_internal_source(obj)


def is_receiver(obj):
    return get_object_type(obj) == 'RECEIVER'


def is_point_receiver(obj):
    return is_receiver(obj) and get_object_subtype(obj) == 'POINT'


def is_source(obj):
    return get_object_type(obj) == 'SOURCE'


def is_altimetry(obj):
    return get_object_type(obj) == 'ALTIMETRY'


def is_altimetry_mesh(obj):
    return is_altimetry(obj) and get_object_subtype(obj) == 'ALTIMETRY MESH'


def is_level_curve(obj):
    return is_altimetry(obj) and get_object_subtype(obj) == 'LEVEL CURVE'


def is_ground_set(obj):
    return is_altimetry(obj) and get_object_subtype(obj) == 'GROUND SET'


def is_water_body(obj):
    return is_altimetry(obj) and get_object_subtype(obj) == 'WATER BODY'


def is_reference_image(obj):
    return get_object_type(obj) == 'REFERENCE_IMAGE'


def is_topographic_map(obj):
    return (
        is_reference_image(obj) and
        get_object_subtype(obj) == "TOPOGRAPHIC_MAP"
    )


def is_mass_plan(obj):
    return is_reference_image(obj) and get_object_subtype(obj) == "MASS_PLAN"


def is_reference_image_poll(self, obj):
    return is_reference_image(obj)


def is_undefined(obj):
    return get_object_type(obj) == 'UNDEFINED'


def use_emissivity(obj):
    return obj.tympan_use_emissivity


def can_be_emissive(obj):
    return obj and is_infrastructure(obj) and not is_screen(obj)


def is_building_poll(self, obj):
    return is_building(obj)


def get_internal_sources(building):
    internal_sources = []
    for obj in bpy.data.objects:
        if is_internal_source(obj) and obj.parent_building == building:
            internal_sources.append(obj)
    return internal_sources


def make_acoustic_source(context, name=None):
    bpy.ops.object.add(type="EMPTY")
    o = context.active_object
    set_object_type(o, 'SOURCE')
    if name is None:
        name = 'Sound source.000'
    o.name = name
    addon_prefs = bpy.context.preferences.addons['blty'].preferences
    o.empty_display_size = addon_prefs.source_size
    o.empty_display_type = 'SPHERE'
    o.tympan_elevation = 2
    spectrum = o.tympan_soundsource_properties.add().spectrum
    tu.initialise_spectrum(spectrum)
    return o


def make_acoustic_receiver(context, name=None):
    bpy.ops.object.add(type="EMPTY")
    o = context.active_object
    set_object_type(o, 'RECEIVER')
    if name is None:
        name = 'Sound receiver.000'
    o.name = name
    addon_prefs = bpy.context.preferences.addons['blty'].preferences
    o.empty_display_size = addon_prefs.receiver_size
    o.empty_display_type = 'SPHERE'
    o.tympan_elevation = 2
    spectrum = o.tympan_soundreceiver_properties.add().spectrum
    tu.initialise_spectrum(spectrum)
    return o


def to_infrastructure(obj, infra_type):
    set_object_type(obj, "INFRASTRUCTURE")
    set_object_subtype(obj, infra_type)
    obj.data.materials.append(tu.create_default_acoustic_material())
    tu.origin_to_bottom(obj)


def to_reference_image(obj, reference_type):
    if is_undefined(obj):
        set_object_type(obj, "REFERENCE_IMAGE")
        set_object_subtype(obj, reference_type)

    def constrain_transforms():
        bpy.ops.object.location_clear()
        obj.lock_location = (False, False, True)
        bpy.ops.object.rotation_clear()
        obj.lock_rotation = (True, True, False)
        bpy.ops.object.scale_clear()
        obj.lock_scale = (True, True, True)

    def display_image_as_single_sided_plane():
        obj.empty_image_depth = 'DEFAULT'
        obj.empty_image_side = 'FRONT'
        # Let the user change the image opacity
        obj.use_empty_image_alpha = True

    def put_mass_plan_above_topographic_map():
        obj.location.z = 0.001

    def put_topographic_map_below_mass_plan():
        obj.location.z = 0

    def make_mass_plan_transparent():
        obj.color[3] = 0.7

    def make_topographic_map_opaque():
        obj.color[3] = 1

    constrain_transforms()
    display_image_as_single_sided_plane()
    if reference_type == 'MASS_PLAN':
        put_mass_plan_above_topographic_map()
        make_mass_plan_transparent()
        obj.name = 'Mass Plan'
    else:
        put_topographic_map_below_mass_plan()
        make_topographic_map_opaque()
        obj.name = 'Topographic Map'


def update_tympan_subtype(self, context):
    if is_undefined(self):
        if self.tympan_subtype in subtypes('INFRASTRUCTURE'):
            to_infrastructure(self, self.tympan_subtype)
    if self.tympan_subtype in subtypes('REFERENCE_IMAGE'):
        to_reference_image(self, self.tympan_subtype)


def register():

    def type_items(self, context):
        type_list = []
        icons = _TYMPAN_TYPES
        type_list.append(("UNDEFINED", "Other",
                          "Object not linked to a tympan_type",
                          icons["UNDEFINED"], 0))
        if self.type == "MESH":
            type_list.append(("INFRASTRUCTURE", "Infrastructure",
                              "Infrastructure", icons["INFRASTRUCTURE"], 1))
            type_list.append(("ALTIMETRY", "Altimetry", "Altimetry",
                              icons["ALTIMETRY"], 2))
        if self.type == "EMPTY":
            type_list.append(("SOURCE", "Sound Source", "Sound source",
                              icons["SOURCE"], 3))
            type_list.append(("RECEIVER", "Receiver", "Sound receiver",
                             icons["RECEIVER"], 4))
            if self.empty_display_type == "IMAGE":
                type_list.append(("REFERENCE_IMAGE", "Reference Image",
                                  "Reference Image", icons["REFERENCE_IMAGE"],
                                  5))
        return tuple(type_list)

    setattr(bpy.types.Object, _OBJECT_TYPE_KEY,
            bpy.props.EnumProperty(items=type_items))

    def subtype_items(self, context):
        tympan_type = get_object_type(self)
        icons = _TYMPAN_SUBTYPES[tympan_type]
        if is_infrastructure(self):
            subtypes = (
                ("BUILDING", "Building", "Building", icons["BUILDING"], 0),
                ("MACHINE", "Machine", "Machine", icons["MACHINE"], 1),
            )
            if not use_emissivity(self):
                screen = (("SCREEN", "Screen", "Screen", icons["SCREEN"], 2),)
                subtypes += screen
            return subtypes
        elif is_receiver(self):
            return (("POINT", "Point Receiver", "Point receiver",
                     icons["POINT"], 0),)
        elif is_source(self):
            return (("POINT", "Point Sound Source", "Point SoundSource",
                     icons["POINT"], 0),)
        elif is_altimetry(self):
            return (
                ("LEVEL CURVE", "Level curve",
                 "Level curve (contour line, topographic profile, ...)",
                 icons["LEVEL CURVE"], 0),
                ("ALTIMETRY MESH", "Altimetry mesh", "Altimetry mesh",
                 icons["ALTIMETRY MESH"], 1),
                ("GROUND SET", "Ground Set", "Ground set",
                 icons["GROUND SET"], 2),
                ("WATER BODY", "Water Body", "Water body",
                 icons["WATER BODY"], 3),
            )
        elif is_reference_image(self):
            return (
                ("TOPOGRAPHIC_MAP", "Topographic Map", "Topographic map",
                 icons["TOPOGRAPHIC_MAP"], 1),
                ("MASS_PLAN", "Mass Plan", "Mass plan", icons["MASS_PLAN"], 2),
            )
        else:
            assert is_undefined(self)
            icons = _TYMPAN_SUBTYPES["INFRASTRUCTURE"]
            if self.type == 'MESH':
                return (
                    ("BUILDING", "To Building", "Building", icons["BUILDING"],
                     1),
                    ("MACHINE", "To Machine", "Machine", icons["MACHINE"], 2),
                    ("SCREEN", "To Screen", "Screen", icons["SCREEN"], 3),
                )
            elif self.type == 'EMPTY' and self.empty_display_type == 'IMAGE':
                icons = _TYMPAN_SUBTYPES["REFERENCE_IMAGE"]
                return (
                    ("TOPOGRAPHIC_MAP", "To Topographic Map",
                     "Topographic map", icons["TOPOGRAPHIC_MAP"], 1),
                    ("MASS_PLAN", "To Mass Plan", "Mass plan",
                     icons["MASS_PLAN"], 2),
                )

    setattr(
        bpy.types.Object,
        _OBJECT_SUBTYPE_KEY,
        bpy.props.EnumProperty(
            items=subtype_items,
            update=update_tympan_subtype,
        ),
    )


def unregister():
    delattr(bpy.types.Object, _OBJECT_TYPE_KEY)
