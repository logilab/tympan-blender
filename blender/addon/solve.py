import configparser
import os
from os import path

import bpy
import json
import numpy as np

from . import (
    core,
    element_types as et,
    infrastructure as iftr,
    io_html_result,
    results,
    tympan_utils as tu,
)


def get_spectrum(sound_property):
    """Use the spectrum object of code_tympan"""
    # Local import for easier testing without Code Tympan.
    from tympan.models import Spectrum
    spectrum = np.array(tu.spectrum_amplitudes(sound_property.spectrum))
    return Spectrum(spectrum)


def get_emissive_infra_result_spectra(context, infra, result):
    # Local import for easier testing without Code Tympan.
    from tympan.models import Spectrum
    spectra = []
    for rec in core.find_used_soundreceivers(context):
        r_index = rec.tympan_solver_index[0].index
        sp_cumul = sum(result.spectrum(r_index, idx.index).values
                       for idx in infra.tympan_solver_index)
        # we convert the spectrum into DB and we add
        # -10 * log10 (Number of sources) to the sum for calculate
        # the DB mean of the contributions.
        spectrum = 10.0 * np.log10(((sp_cumul + 10e-15) / 4.0e-10) /
                                   len(infra.tympan_solver_index))
        spectra.append(Spectrum(spectrum))
    return spectra


def get_emissive_infra_source_spectra(infra):
    # Local import for easier testing without Code Tympan.
    from tympan.models import Spectrum
    spectra = []
    if iftr.has_emitting_faces(infra):
        for group in infra.tympan_discretization_groups:
            if et.is_machine(infra):
                spectra.append(get_spectrum(group).values)
            else:  # building
                spectra.append(
                    iftr.calc_building_discretization_group_spectrum(
                        infra,
                        group,
                    )
                )
    sp_cumul = sum(sp for sp in spectra)
    return Spectrum(
        10.0 * np.log10((sp_cumul + 10e-15) / 4.0e-10) / len(spectra)
    )


def create_tympan_result(context, result):
    """formatting results for display facilities."""
    src_spectra = {}
    rec_spectra = {}
    couples_spectra = {}
    for rec in core.find_used_soundreceivers(context):
        rec_spectrum = rec.tympan_soundreceiver_properties[0]
        rec_spectra[rec.name] = get_spectrum(rec_spectrum)
    for src in core.find_used_soundsources(context):
        s_index = src.tympan_solver_index[0].index
        src_spectra[src.name] = get_spectrum(
            src.tympan_soundsource_properties[0]
        )
        for rec in core.find_used_soundreceivers(context):
            r_index = rec.tympan_solver_index[0].index
            couples_spectra[(src.name, rec.name)] = result.spectrum(r_index,
                                                                    s_index)
    for infra in iftr.find_emitting_infrastructures(context):
        src_spectra[infra.name] = get_emissive_infra_source_spectra(infra)
        spectra = get_emissive_infra_result_spectra(context, infra, result)
        for rec, sp in zip(core.find_used_soundreceivers(context), spectra):
            couples_spectra[(infra.name, rec.name)] = sp
    return results.TympanResults(src_spectra, rec_spectra, couples_spectra)


def manage_result(context, result, result_file_path):
    """Write the results in the HTML result file."""
    result_data = create_tympan_result(context, result)
    exporter = io_html_result.TympanResultExporter(result_data)
    exporter.write_html_result(result_file_path)


def append_in_tympan_solver_index(obj, index):
    """Append a new index in the object tympan_solver_index collection"""
    obj.tympan_solver_index.add().index = index


def materials_by_face_from_altimesh(altimetry, report):
    material_by_face = altimetry.material_by_face
    faces_material = altimetry.mesh.faces_material(material_by_face)
    materials = []
    missing_mat = False
    for face_material in faces_material:
        resistivity = face_material.resistivity
        deviation = 0
        length = 0.001
        if resistivity is None:
            missing_mat = True
            default_ground_props = bpy.context.scene.default_ground
            resistivity = default_ground_props.resistivity
            length = default_ground_props.length
            deviation = default_ground_props.deviation
        material = [
            face_material.id,
            resistivity,
            deviation,
            length,
        ]
        materials.append(material)
    if missing_mat and report is not None:
        report({'WARNING'},
               'Some faces of the altimetry have no resistivity, '
               'using default material.')
    return materials


def build_equivalent_soundsources_tympan_directivity(dtype, normal, area):
    from tympan.models._solver import Directivity
    return Directivity(
        directivity_type=dtype,
        support_normal=normal,
        size=area,
    )


def build_model(altimetry, infras=(), sources=(), receivers=(), report=None):
    """Build a solver model."""
    # Local import for easier testing without Code Tympan.
    from tympan.models import Spectrum
    from tympan.models.solver import Model, Source, Receptor
    model = Model()
    vertices, faces = altimetry.mesh.as_arrays()
    materials = materials_by_face_from_altimesh(altimetry, report)
    model.add_mesh(vertices, faces, materials)
    for (vertices, faces), material in infras:
        mat = [iftr.get_acoustic_material(material)] * len(faces)
        model.add_mesh(vertices, faces, mat)
    for src, position, spectrum, directivity in sources:
        if directivity:
            directivity = build_equivalent_soundsources_tympan_directivity(
                directivity['type'],
                directivity['normal'],
                directivity['area'],
            )
        solver_index = model.add_source(
            Source(position, Spectrum(np.array(spectrum)), directivity))
        append_in_tympan_solver_index(src, solver_index)
    if model.nsources <= 0:
        if report is not None:
            report({'WARNING'}, 'model has no source')
    for rec in receivers:
        solver_index = model.add_receptor(Receptor(tuple(rec.location[:])))
        append_in_tympan_solver_index(rec, solver_index)
    if model.nreceptors <= 0:
        if report is not None:
            report({'WARNING'}, 'model has no receptor')
    return model


def run_solver(solver_name, model, report=None):
    # Local import for easier testing without Code Tympan.
    from tympan.models.solver import Solver
    try:
        tympan_solver_dir = os.environ['TYMPAN_SOLVERDIR']
    except KeyError:
        raise RuntimeError(
            'TYMPAN_SOLVERDIR environment variable is not defined')
    params = path.join(os.getenv('TYMPAN_TMP_DIR'), 'solverparams.ini')
    solver = Solver.from_name(solver_name, params, tympan_solver_dir)
    if report is not None:
        report({'DEBUG'},
               'running "{}" solver with parameters from {}'.format(
                   solver_name, params))
    if report is not None:
        report({'INFO'},
               '"{}" solver terminated successfully'.format(solver_name))
    return solver.solve(model)


def read_parameters_file():
    solver_parameters_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'solverparams.json',
    )
    with open(solver_parameters_file, 'r') as f:
        parameters_groups = json.load(f)
    return parameters_groups


def write_parameters_file(solver_properties):
    params = configparser.ConfigParser()
    params.optionxform = str
    for category, parameters in solver_properties.parameters_groups.items():
        params[category] = {}
        for param_name in parameters:
            param_value = getattr(solver_properties, param_name)
            if isinstance(param_value, float):
                param_value = round(param_value, 5)
            params[category][param_name] = str(param_value)
    param_file = os.path.join(
        os.getenv('TYMPAN_TMP_DIR'),
        'solverparams.ini',
    )
    with open(param_file, 'w') as param_file:
        params.write(param_file, space_around_delimiters=False)


def init_properties(cls):
    """Load properties dynamicaly from json parameter file"""
    cls.__annotations__ = {}
    cls.parameters_groups = read_parameters_file()
    property_mapping = {
        'bool': bpy.props.BoolProperty,
        'float': bpy.props.FloatProperty,
        'double':  bpy.props.FloatProperty,
        'int': bpy.props.IntProperty,
    }
    for category, parameters in cls.parameters_groups.items():
        for param_name, details in parameters.items():
            param_type = details['type']
            cls.__annotations__[param_name] = property_mapping[param_type](
                name='',
                default=details['default'],
                description=details['help'],
                options=set(),
            )
    cls.__annotations__['active_param_category'] = bpy.props.EnumProperty(
        items=tuple([(c, c, c) for c in cls.parameters_groups])
    )
    return cls


@init_properties
class SolverProperties(bpy.types.PropertyGroup):
    pass


class DefaultGroundProperties(bpy.types.PropertyGroup):
    resistivity: bpy.props.FloatProperty(default=20000, min=0, options=set())
    deviation: bpy.props.FloatProperty(default=0, options=set())
    length: bpy.props.FloatProperty(
        default=0.001, precision=3, min=0, options=set()
    )


def register():
    bpy.utils.register_class(SolverProperties)
    bpy.utils.register_class(DefaultGroundProperties)
    bpy.types.Scene.solver_properties = bpy.props.PointerProperty(
        type=SolverProperties
    )
    bpy.types.Scene.default_ground = bpy.props.PointerProperty(
        type=DefaultGroundProperties
    )


def unregister():
    bpy.utils.unregister_class(DefaultGroundProperties)
    bpy.utils.unregister_class(SolverProperties)
    del bpy.types.Scene.default_ground
    del bpy.types.Scene.solver_properties
