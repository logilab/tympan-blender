import bpy


class ShaderNodeLayersWrapper:
    """Given the color output of a node, adds a layers on top of it.
    Each added layer has an unbount scala input that can be used to set its
    opacity.

    Create the shader:
    >>> layers_wrapper = ShaderNodeLayersWrapper(mat, base_color_output)
    >>> layers_wrapper.add_layer("Layer1", color_src=texture_color_output)

    Display the layer opacity field in a panel:
    >>> layout.prop(material.node_tree.nodes["Layer1"].inputs[0])
    """

    OPACITY_NODE_GROUP_NAME = "OpacityController"

    def __init__(self, material, base_color):
        self.node_tree = material.node_tree
        self.nodes = self.node_tree.nodes
        self.links = self.node_tree.links
        base_color = self.nodes.new("ShaderNodeRGB")
        base_color.outputs[0].default_value = (0.1, 0.1, 0.1, 1)

        self._color_output = base_color.outputs[0]

    def add_layer(self, name, color_src, alpha_src=None, initial_opacity=0.5):
        """Add a texture as a layer to the altimetry shader.
        The texture opacity can be controlled through the first input of the
        node named `name`.
        `color_src` must be the color output of the source texture.
        `alpha_src` must be the alpha output of the source texture. When None,
        the texture is considered to be opaque.
        The created nodes equivalent equation is:
        color_output = mixrgb(
            color1=color_output,
            color2=color_src,
            factor=alpha_src * initial_opacity
        )
        """
        nodes, links = self.nodes, self.links
        layer_grp = self._get_layer_node_group()
        layer = nodes.new("ShaderNodeGroup")
        layer.node_tree = layer_grp
        layer.name = name
        layer.inputs[0].default_value = initial_opacity
        links.new(layer.inputs[1], color_src)
        if alpha_src is not None:
            links.new(layer.inputs[2], alpha_src)
        links.new(layer.inputs[3], self._color_output)
        self._color_output = layer.outputs[0]

    def _get_layer_node_group(self):
        if self.OPACITY_NODE_GROUP_NAME not in bpy.data.node_groups:
            return self._create_layer_node_group()
        return bpy.data.node_groups[self.OPACITY_NODE_GROUP_NAME]

    def _create_layer_node_group(self):
        """Create a node group strictly equivalent to a ShaderNodeMath with
        "MULTIPLY" operator with its first scale input constrained to the
        domain [0,1], allowing to display it in a panel without being able to
        choose a value in the whole floating number range.

        Input 0: Opacity to apply to the top layer (float in [0, 1])
        Input 1: Color channel of the top layer (color)
        Input 2: Alpha channel of the top layer (float in [0, 1])
        Input 3: Color channel of the destination layer (color)

        Output 0: Color channel of the mixed layers (color)
        """
        # Inputs declaration
        group = bpy.data.node_groups.new(self.OPACITY_NODE_GROUP_NAME,
                                         "ShaderNodeTree")
        nodes = group.nodes
        links = group.links
        group_inputs = nodes.new("NodeGroupInput")
        opacity_input = group.inputs.new('NodeSocketFloat', 'opacity')
        opacity_input.min_value = 0
        opacity_input.max_value = 1
        group.inputs.new('NodeSocketColor', 'source color')
        src_alpha = group.inputs.new('NodeSocketFloat', 'source alpha')
        group.inputs.new('NodeSocketColor', 'destination color')

        # Make textures without alpha channel fully opaque
        src_alpha.default_value = 1

        # Outputs declaration
        group_outputs = nodes.new("NodeGroupOutput")
        group.outputs.new('NodeSocketColor', 'color')

        # Mutliply opacity by alpha of top layer
        alpha_mult = nodes.new("ShaderNodeMath")
        alpha_mult.operation = "MULTIPLY"
        links.new(alpha_mult.inputs[0], group_inputs.outputs["opacity"])
        links.new(alpha_mult.inputs[1], group_inputs.outputs["source alpha"])

        # Use computed alpha to mix source and destination colors
        mix = nodes.new("ShaderNodeMixRGB")
        links.new(mix.inputs[0], alpha_mult.outputs[0])
        links.new(mix.inputs[1], group_inputs.outputs["destination color"])
        links.new(mix.inputs[2], group_inputs.outputs["source color"])
        group.links.new(group_outputs.inputs["color"], mix.outputs[0])
        return group

    @property
    def color_output(self):
        return self._color_output
