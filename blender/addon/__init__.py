import bpy

bl_info = {
    "name": "Code_Tympan",
    "author": "Logilab SA",
    "blender": (2, 83, 0),
    "location": "https://forge.extranet.logilab.fr/open-source/tympan-blender",
    "description":
    """Code_TYMPAN is an open source software dedicated to the simulation of \
    the propagation of the industrial noise in the environment.  It allows to \
    deal with 3D realistic geometries and has a convenient Graphical User \
    Interface (GUI) to help engineers to build 3D models and to achieve \
    analysis needed in environmental noise studies.""",
    "wiki": "http://code-tympan.org/",
    "warning": "",
    "tracker_url": "",
    "category": "User Interface",
}


class BltyPreferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    source_size: bpy.props.FloatProperty(name="Sources Size", default=1,
                                         unit="LENGTH")
    receiver_size: bpy.props.FloatProperty(name="Receivers Size", default=1,
                                           unit="LENGTH")
    building_size: bpy.props.FloatProperty(name="Buildings Size", default=10,
                                           unit="LENGTH")
    image_size: bpy.props.FloatProperty(name="Images Size", default=100,
                                        unit="LENGTH")

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        row = layout.row()
        row.prop(self, 'source_size')
        row.prop(self, 'receiver_size')
        row = layout.row()
        row.prop(self, 'building_size')
        row = layout.row()
        row.prop(self, 'image_size')


def register():
    bpy.utils.register_class(BltyPreferences)
    from . import collectionwrapper
    collectionwrapper.register_handlers()
    from . import element_types
    from . import infrastructure
    from . import core
    from . import ui
    from . import io_spectrum
    from . import io_html_result
    from . import io_xml_project
    from . import altimetry
    from . import reference_images
    from . import solve
    element_types.register()
    infrastructure.register()
    core.register()
    ui.register()
    io_html_result.register()
    io_xml_project.register()
    io_spectrum.register()
    altimetry.register()
    reference_images.register()
    solve.register()


def unregister():
    bpy.utils.unregister_class(BltyPreferences)
    from . import reference_images
    from . import altimetry
    from . import io_spectrum
    from . import core
    from . import ui
    from . import infrastructure
    from . import element_types
    from . import io_html_result
    from . import io_xml_project
    from . import solve
    solve.unregister()
    reference_images.unregister()
    altimetry.unregister()
    io_spectrum.unregister()
    io_html_result.unregister()
    io_xml_project.unregister()
    core.unregister()
    ui.unregister()
    infrastructure.unregister()
    element_types.unregister()
