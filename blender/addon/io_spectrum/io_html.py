from bokeh.layouts import (
    column,
    widgetbox,
)
from bokeh.models import (
    ColumnDataSource,
)
from bokeh.models.widgets import (
    Div,
)
from bokeh.plotting import (
    output_file,
    save,
)
import bpy
from bpy.types import Operator
import os
import tempfile

from blty import (
    bokeh_utils,
    tympan_utils as tu,
)
from blty.io_spectrum.utils import (
    find_active_spectrum,
    find_active_emission_spectrum,
    find_active_parameter_spectrum,
)


def create_spectrum_histogram(obj_name, spectrum, outputfile_path):
    """Create an HTML page with the histogram of the spectrum associated
    with obj (depending on its type)."""
    document_title = widgetbox(
        Div(
            text="""<h1>Spectrum of {}<h1>""".format(obj_name),
            style={'white-space': 'nowrap'},
        )
    )
    frequencies = [b.frequency for b in spectrum.bands]
    data = {
        'freq': frequencies,
        'values': [b.amplitude for b in spectrum.bands],
        'left': frequencies,
        'right': frequencies[1:] + [20000],
    }
    histogram = bokeh_utils.create_histogram(ColumnDataSource(data))
    output_file(str(outputfile_path))
    save(column(document_title, histogram))


class TympanDisplaySpectrum(Operator):
    bl_label = 'Display Spectrum'
    bl_idname = tu.blty_idname('display_spectrum')
    bl_description = "Display the spectrum's histogram"

    @classmethod
    def poll(cls, context):
        return find_active_spectrum(context)

    def execute(self, context):
        obj = context.active_object
        with tempfile.NamedTemporaryFile(delete=False,
                                         dir=os.getenv('TYMPAN_TMP_DIR'),
                                         suffix=".html") as temp_spectrum_file:
            outputfile_path = temp_spectrum_file.name
            spectrum = find_active_spectrum(context)
            create_spectrum_histogram(obj.name, spectrum, outputfile_path)
        bpy.ops.wm.path_open(filepath=outputfile_path)
        return {'FINISHED'}


class TympanDisplayEmissionSpectrum(Operator):
    bl_label = 'Display Spectrum'
    bl_idname = tu.blty_idname('display_emission_spectrum')
    bl_description = "Display the spectrum's histogram"

    @classmethod
    def poll(cls, context):
        return find_active_emission_spectrum(context)

    def execute(self, context):
        obj = context.active_object
        with tempfile.NamedTemporaryFile(delete=False,
                                         dir=os.getenv('TYMPAN_TMP_DIR'),
                                         suffix=".html") as temp_spectrum_file:
            outputfile_path = temp_spectrum_file.name
            spectrum = find_active_emission_spectrum(context)
            create_spectrum_histogram(obj.name, spectrum, outputfile_path)
        bpy.ops.wm.path_open(filepath=outputfile_path)
        return {'FINISHED'}


class TympanDisplayParameterSpectrum(Operator):
    bl_label = 'Display Spectrum'
    bl_idname = tu.blty_idname('display_parameter_spectrum')
    bl_description = "Display the spectrum's histogram"

    @classmethod
    def poll(cls, context):
        return find_active_parameter_spectrum(context)

    def execute(self, context):
        obj = context.active_object
        with tempfile.NamedTemporaryFile(delete=False,
                                         dir=os.getenv('TYMPAN_TMP_DIR'),
                                         suffix=".html") as temp_spectrum_file:
            outputfile_path = temp_spectrum_file.name
            spectrum = find_active_parameter_spectrum(context)
            create_spectrum_histogram(obj.name, spectrum, outputfile_path)
        bpy.ops.wm.path_open(filepath=outputfile_path)
        return {'FINISHED'}
