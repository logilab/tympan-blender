from blty import (
    infrastructure,
)


def user_can_update_active_spectrum(context):
    return _find_active_spectrum_holder(context).is_user_updatable


def user_can_update_active_emission_spectrum(context):
    return _find_active_emission_spectrum_holder(context).is_user_updatable


def _find_active_spectrum_holder(context):
    obj = context.active_object
    if obj is None:
        return None

    spectrum_holders = (
        "tympan_soundsource_properties",
        "tympan_soundreceiver_properties",
    )
    for prop in spectrum_holders:
        prop = getattr(obj, prop)
        if len(prop) == 1:
            return prop[0]
    if obj.active_material:
        return getattr(obj.active_material, 'acoustic_properties')


def _find_active_emission_spectrum_holder(context):
    obj = context.active_object
    if obj is None or not infrastructure.has_emitting_faces(obj):
        return None
    active_index = obj.active_discretization_group_index
    return obj.tympan_discretization_groups[active_index]


def _find_active_parameter_spectrum_holder(context):
    obj = context.active_object
    if obj is None:
        return None
    parameter_spectrum_holders = (
        "raw_isolation",
        "sabine_inner_absorption",
        "sabine_outer_absorption",
    )
    active_index = obj.active_discretization_group_index
    parameters_properties = obj.tympan_discretization_groups[active_index]
    if parameters_properties:
        for prop in parameter_spectrum_holders:
            if prop == parameters_properties.active_absorption_parameter:
                spectrum_prop = getattr(parameters_properties, prop)
                return spectrum_prop


def find_active_spectrum(context):
    holder = _find_active_spectrum_holder(context)
    if holder:
        return holder.spectrum


def find_active_emission_spectrum(context):
    holder = _find_active_emission_spectrum_holder(context)
    if holder:
        return holder.spectrum


def find_active_parameter_spectrum(context):
    holder = _find_active_parameter_spectrum_holder(context)
    if holder:
        return holder.spectrum
