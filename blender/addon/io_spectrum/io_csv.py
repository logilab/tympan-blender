import csv
import io
from pathlib import Path

import pyperclip

import bpy.props
from bpy.types import (
    Operator,
)
from bpy_extras.io_utils import (
    ExportHelper,
    ImportHelper,
)

from blty import (
    tympan_utils as tu,
)
from blty.io_spectrum.utils import (
    find_active_spectrum,
    find_active_emission_spectrum,
    find_active_parameter_spectrum,
    user_can_update_active_spectrum,
    user_can_update_active_emission_spectrum,
)


class CSVSpectrumExporter:

    def __init__(self, frequencies_label, amplitudes_label):
        self._field_names = (frequencies_label, amplitudes_label)

    def export_spectrum(self, fileobj, frequencies, amplitudes):
        writer = csv.writer(fileobj, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(self._field_names)
        writer.writerows(zip(frequencies, amplitudes))


class ExportSpectrum:

    #  ExporterHelper's attributes
    filename_ext = '.csv'
    filter_glob: bpy.props.StringProperty(
        default="*.csv",
        options={'HIDDEN'},
    )

    frequencies_label: bpy.props.StringProperty(
        name='Frequencies Label',
        default='Frequency (Hz)',
    )
    amplitudes_label: bpy.props.StringProperty(
        name='Level Label',
        default='Level (dB)',
    )

    def get_spectum(self, context):
        raise NotImplementedError

    def execute(self, context):
        exporter = CSVSpectrumExporter(
            self.frequencies_label,
            self.amplitudes_label,
        )
        spectrum = self.get_spectrum(context)
        frequencies, amplitudes = zip(*[(band.frequency, band.amplitude)
                                        for band in spectrum.bands])

        with Path(self.filepath).open('w', newline='', encoding='utf-8') as f:
            exporter.export_spectrum(f, frequencies, amplitudes)
        return {'FINISHED'}


class TympanExportCSVSpectrum(Operator, ExportSpectrum, ExportHelper):
    bl_label = 'Export CSV'
    bl_idname = tu.blty_idname('export_spectrum_as_csv')
    bl_description = 'Export spectrum as CSV'

    @classmethod
    def poll(cls, context):
        return find_active_spectrum(context)

    def get_spectrum(self, context):
        return find_active_spectrum(context)


class TympanExportCSVEmissionSpectrum(Operator, ExportSpectrum, ExportHelper):
    bl_label = 'Export Emission CSV'
    bl_idname = tu.blty_idname('export_emission_spectrum_as_csv')
    bl_description = 'Export emission spectrum as CSV'

    @classmethod
    def poll(cls, context):
        return find_active_emission_spectrum(context)

    def get_spectrum(self, context):
        return find_active_emission_spectrum(context)


class TympanExportCSVParameterSpectrum(Operator, ExportSpectrum, ExportHelper):
    bl_label = 'Export Parameter CSV'
    bl_idname = tu.blty_idname('export_parameter_spectrum_as_csv')
    bl_description = 'Export parameter spectrum as CSV'

    def get_spectrum(self, context):
        return find_active_parameter_spectrum(context)


class CopySpectrum:

    frequencies_label: bpy.props.StringProperty(
        name='Frequencies Label',
        default='Frequency (Hz)',
    )
    amplitudes_label: bpy.props.StringProperty(
        name='Level Label',
        default='Level (dB)',
    )

    def get_spectrum(self, context):
        raise NotImplementedError

    def execute(self, context):
        exporter = CSVSpectrumExporter(
            self.frequencies_label,
            self.amplitudes_label,
        )
        f = io.StringIO(newline=None)
        spectrum = self.get_spectrum(context)
        frequencies, amplitudes = zip(*[(band.frequency, band.amplitude)
                                        for band in spectrum.bands])

        exporter.export_spectrum(f, frequencies, amplitudes)
        pyperclip.copy(f.getvalue())
        return {'FINISHED'}


class TympanCopyCSVSpectrum(Operator, CopySpectrum):
    bl_label = 'Copy Spectrum'
    bl_idname = tu.blty_idname('copy_spectrum_as_csv')
    bl_description = 'Copy spectrum as CSV'

    @classmethod
    def poll(cls, context):
        return find_active_spectrum(context)

    def get_spectrum(self, context):
        return find_active_spectrum(context)


class TympanCopyCSVEmissionSpectrum(Operator, CopySpectrum):
    bl_label = 'Copy Emission Spectrum'
    bl_idname = tu.blty_idname('copy_emission_spectrum_as_csv')
    bl_description = 'Copy emission spectrum as CSV'

    @classmethod
    def poll(cls, context):
        return find_active_emission_spectrum(context)

    def get_spectrum(self, context):
        return find_active_emission_spectrum(context)


class TympanCopyCSVParameterSpectrum(Operator, CopySpectrum):
    bl_label = 'Copy Parameter Spectrum'
    bl_idname = tu.blty_idname('copy_parameter_spectrum_as_csv')
    bl_description = 'Copy parameter spectrum as CSV'

    def get_spectrum(self, context):
        return find_active_parameter_spectrum(context)


class ImportSpectrum:

    filter_glob: bpy.props.StringProperty(
        default="*.csv",
        options={'HIDDEN'},
    )

    def get_spectum(self, context):
        raise NotImplementedError

    def execute(self, context):
        with Path(self.filepath).open('r', newline='', encoding='utf-8') as f:
            spectrum = self.get_spectrum(context)
            try:
                amplitudes = import_csv(context, f)
                tu.initialise_spectrum(spectrum, values=amplitudes)
            except ValueError:
                self.report({'ERROR'},
                            'Imported csv contains invalid amplitudes.')
                return {'CANCELLED'}
            return {'FINISHED'}


class TympanImportCSVSpectrum(Operator, ImportSpectrum, ImportHelper):
    bl_label = 'Import CSV'
    bl_idname = tu.blty_idname('import_spectrum_from_csv')
    bl_description = 'Import spectrum from CSV'

    @classmethod
    def poll(cls, context):
        return user_can_update_active_spectrum(context)

    def get_spectrum(self, context):
        return find_active_spectrum(context)


class TympanImportCSVEmissionSpectrum(Operator, ImportSpectrum, ImportHelper):
    bl_label = 'Import Emission CSV'
    bl_idname = tu.blty_idname('import_emission_spectrum_from_csv')
    bl_description = 'Import emission spectrum from CSV'

    @classmethod
    def poll(cls, context):
        return user_can_update_active_emission_spectrum(context)

    def get_spectrum(self, context):
        return find_active_emission_spectrum(context)


class TympanImportCSVParameterSpectrum(Operator, ImportSpectrum, ImportHelper):
    bl_label = 'Import Parameter CSV'
    bl_idname = tu.blty_idname('import_parameter_spectrum_from_csv')
    bl_description = 'Import parameter spectrum from CSV'

    def get_spectrum(self, context):
        return find_active_parameter_spectrum(context)


class PasteSpectrum:

    def get_spectum(self, context):
        raise NotImplementedError

    def execute(self, context):
        clipboard_content = pyperclip.paste()
        if not clipboard_content:
            self.report({'ERROR'}, 'Nothing to paste in clipboard.')
            return {'CANCELLED'}

        fileobj = io.StringIO(clipboard_content)
        spectrum = self.get_spectrum(context)
        try:
            amplitudes = import_csv(context, fileobj)
            tu.initialise_spectrum(spectrum, values=amplitudes)
        except ValueError:
            self.report({'ERROR'},
                        'Pasted csv contains invalid amplitudes.')
            return {'CANCELLED'}
        return {'FINISHED'}


class TympanPasteSpectrum(Operator, PasteSpectrum):
    bl_label = 'Paste Spectrum'
    bl_idname = tu.blty_idname('paste_spectrum')
    bl_description = 'Paste spectrum from clipboard'

    @classmethod
    def poll(cls, context):
        return user_can_update_active_spectrum(context)

    def get_spectrum(self, context):
        return find_active_spectrum(context)


class TympanPasteEmissionSpectrum(Operator, PasteSpectrum):
    bl_label = 'Paste Emission Spectrum'
    bl_idname = tu.blty_idname('paste_emission_spectrum')
    bl_description = 'Paste emission spectrum from clipboard'

    @classmethod
    def poll(cls, context):
        return user_can_update_active_emission_spectrum(context)

    def get_spectrum(self, context):
        return find_active_emission_spectrum(context)


class TympanPasteParameterSpectrum(Operator, PasteSpectrum):
    bl_label = 'Paste Parameter Spectrum'
    bl_idname = tu.blty_idname('paste_parameter_spectrum')
    bl_description = 'Paste parameter spectrum from clipboard'

    def get_spectrum(self, context):
        return find_active_parameter_spectrum(context)


def import_csv(context, fileobj):
    try:
        dialect = csv.Sniffer().sniff(fileobj.read(1024))
    except csv.Error:
        dialect = 'excel'
    fileobj.seek(0)
    csv_reader = csv.reader(fileobj, dialect=dialect)
    columns = list(zip(*csv_reader))
    amplitudes_column = columns[-1]
    try:
        float(amplitudes_column[0])
        amplitudes = amplitudes_column
    except ValueError:
        amplitudes = amplitudes_column[1:]
    return list(map(float, amplitudes))
