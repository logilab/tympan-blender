"""IO utilities for tympan Blender addon."""
from collections import namedtuple
from configparser import ConfigParser
import os
from os import path

import bpy
from bpy import ops
from bpy.types import (
    Operator,
    TOPBAR_MT_file_import,
)
from bpy.utils import (
    register_class,
    unregister_class,
)
from bpy_extras.io_utils import ImportHelper

from lxml import etree
from mathutils import (
    Matrix,
    Vector,
)
import numpy as np

from . import (
    altimetry,
    element_types,
    infrastructure as infra,
    io_html_result,
    results,
    tympan_utils as tu,
)


class TympanXMLImport(Operator, ImportHelper):
    bl_idname = tu.blty_idname('xml_import')
    bl_label = 'Code Tympan project (.xml)'
    bl_category = 'Tympan'
    bl_description = 'Load a Tympan xml file'

    # Configure ImportHelper to filter file names.
    filename_ext = ".xml"
    filter_glob: bpy.props.StringProperty(
        default="*.xml",
        options={'HIDDEN'},
    )

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def execute(self, context):
        if not self.filepath:
            self.report({'INFO'}, 'no import file specified')
            return {'CANCELLED'}
        realpath = path.realpath(path.expanduser(self.properties.filepath))
        self.report({'INFO'}, 'importing: {}'.format(realpath))
        xml_importer = XMLImporter(context, realpath)
        xml_importer.import_file()
        return {'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


def menu_draw(self, context):
    # callback to add the operator for file import menu.
    self.layout.operator(TympanXMLImport.bl_idname)


class TympanXMLParseError(Exception):
    """Error while parsing a Code Tympan XML project file."""


def _find(element, tag):
    """Return first child of `element` with `tag` or raise
    TympanXMLParseError.
    """
    child = element.find(tag)
    if child is None:
        raise TympanXMLParseError(
            'no <{}> element found in {}'.format(tag, element)
        )
    return child


def _coords(element):
    return Vector([float(element.get(axis)) for axis in 'xyz'])


def _transform_matrix(xmlroot):
    '''Get the transformation matrix from local the coordinate system.'''
    repere = _find(xmlroot, 'Repere')
    _check_coord_system(repere)
    matrix = Matrix()  # 4x4 identity matrix
    for i, v in enumerate(repere.findall('Vector') + [_find(repere, 'Point')]):
        matrix[i][0:3] = _coords(v)
    matrix.transpose()
    return matrix


def _check_coord_system(repere):
    vectors = repere.findall('Vector')
    if len(vectors) != 3:
        raise TympanXMLParseError(
            'expecting 3 <Vector> element in {} (got {})'.format(
                repere, len(vectors))
        )


def _get_cubic_infra(infra_node):
    '''get walls vertices from geometryNode'''
    walls = {}
    default_emissivity_group = {
        'default': True,
        'props': _get_node_emissivity_properties(infra_node),
        'algorithm': 'uniform',
    }
    acoustic_box_node = infra_node.find('GeometryNode/AcousticBox')
    machine_emissivity_props = _get_node_emissivity_properties(acoustic_box_node)
    if machine_emissivity_props and machine_emissivity_props['distribution_type'] == 1:
        default_emissivity_group['props'] = machine_emissivity_props
    for geonode in acoustic_box_node.findall('GeometryNode'):
        vertices = []
        face_matrix = _transform_matrix(geonode)
        wall_node = geonode.find('AcousticRectangleNode')
        wall_emissivity_props = _get_node_emissivity_properties(wall_node)
        if wall_emissivity_props and wall_emissivity_props['distribution_type'] == 1:
            wall_emissivity_group = {
                'default': False,
                'props': wall_emissivity_props,
                'algorithm': 'uniform',
            }
        else:
            wall_emissivity_group = default_emissivity_group
        wall_name = wall_node.get('name')
        rectangle = wall_node.find('Rectangle')
        for point in rectangle.findall('Point'):
            vertex = face_matrix @ Vector(_coords(point))
            vertices.append(vertex)
        walls[wall_name] = {
            'vertices': vertices,
            'emissivity_group': wall_emissivity_group,
        }
    return walls


def _get_cylindric_infra_walls(infra_node):
    '''get walls (ceiling end floor) vertices coords from geometryNode.
    The cylinders are described by 2 rectangles containing the circles of
    the bases of the cylinder.'''
    walls = {}
    default_emissivity_props = _get_node_emissivity_properties(infra_node)
    acoustic_cylinder_node = infra_node.find('GeometryNode/AcousticCylinder')
    machine_emissivity_props = _get_node_emissivity_properties(acoustic_cylinder_node)
    if machine_emissivity_props and machine_emissivity_props['distribution_type']:
        default_emissivity_props = machine_emissivity_props
    side_wall_node = acoustic_cylinder_node.find('AcousticSurface')
    side_wall_emissivity_props = _get_node_emissivity_properties(side_wall_node)
    side_wall_name = side_wall_node.get('name')
    if side_wall_emissivity_props and side_wall_emissivity_props['distribution_type']:
        walls[side_wall_name] = {
            'vertices': [],
            'emissivity_group': {
                'default': False,
                'props': side_wall_emissivity_props,
                'algorithm': 'uniform',
            },
        }
    else:
        walls[side_wall_name] = {
            'vertices': [],
            'emissivity_group': {
                'default': True,
                'props': default_emissivity_props,
                'algorithm': 'uniform',
            }
        }
    for wall_node in acoustic_cylinder_node.findall('AcousticCircle'):
        wall_name = wall_node.get('name')
        wall_emissivity_props = _get_node_emissivity_properties(wall_node)
        if wall_emissivity_props and wall_emissivity_props['distribution_type']:
            wall_emissivity_group = {
                'default': False,
                'props': wall_emissivity_props,
                'algorithm': 'circular',
            }
        else:
            wall_emissivity_group = {
                'default': False,
                'props': default_emissivity_props,
                'algorithm': 'circular'
            }
        vertices = []
        for point in wall_node.findall('Rectangle/Point'):
            vertices.append(_coords(point))
        walls[wall_name] = {
            'vertices': vertices,
            'emissivity_group': wall_emissivity_group,
        }
    return walls


def _get_screen_infra(screen_node):
    '''Get vertices and faces of the ceiling and the floor of the
    screen.'''
    name = screen_node.get('name')
    height = screen_node.find('hauteur').text
    thickness = screen_node.find('epaisseur').text
    vertices = []
    for polygon in screen_node.findall('squelette'):
        for point in polygon.findall('Point'):
            vertices.append(_coords(point))
    return name, vertices, height, thickness


def _get_polygonal_infra_walls(geonode):
    walls = {}
    # side walls
    for wallnode in geonode.findall('Etage/ListMur/GeometryNode'):
        vertices = []
        face_matrix = _transform_matrix(wallnode)
        wall_name = wallnode.findall('Mur')[0].get('name')
        rectangle = _find(wallnode, 'Mur/Rectangle')
        for point in rectangle.findall('Point'):
            vertex = face_matrix @ Vector(_coords(point))
            vertices.append(vertex)
        walls[wall_name] = {'vertices': vertices}
    # floor and celling
    for slabnode, wall_name in zip(geonode.findall('Etage/Dalle'), ['floor', 'ceiling']):
        vertices = []
        for polygon in slabnode.findall('Polygon'):
            for point in polygon.findall('Point'):
                vertices.append(_coords(point))
        walls[wall_name] = {'vertices': vertices}
    return walls


def _get_node_emissivity_properties(infra_node):
    current_regime = int(infra_node.findtext('curRegime') or 0)
    regimes_node = infra_node.findall('Regime')
    if not regimes_node:
        return
    density_horizontal = infra_node.findtext('densiteSrcsH')
    density_vertical = infra_node.findtext('densiteSrcsV')
    if density_vertical != density_horizontal:
        raise NotImplementedError("can't import a non uniform density")
    density = float(density_horizontal)
    regime_node = regimes_node[current_regime]
    is_emissive = bool(int(regime_node.findtext('isRayonnant')))
    distribution_type = int(regime_node.findtext('typeDistribution'))
    spectrum = None
    if distribution_type == 1:
        spectrum = _parse_spectrum(regime_node.find('Spectre'))
    return {
        'is_emissive': is_emissive,
        'density': density,
        'distribution_type': distribution_type,
        'spectrum': spectrum,
    }


def _load_solver_parameters(solver_properties, solver_params):
    for category, parameters in solver_properties.parameters_groups.items():
        for param, values in parameters.items():
            if param in solver_params[category]:
                new_param_value = solver_params[category][param]
                param_value = getattr(solver_properties, param)
                if isinstance(param_value, float):
                    new_param_value = float(new_param_value)
                elif isinstance(param_value, bool):
                    new_param_value = bool(new_param_value)
                elif isinstance(param_value, int):
                    new_param_value = int(new_param_value)
                setattr(solver_properties, param, new_param_value)


def _to_solver_spectrum(spectrum):
    from tympan.models import Spectrum
    return Spectrum(np.array(spectrum))


def _parse_spectrum(node_spectrum):
    '''parse and check the spectrum from xml'''
    assert float(node_spectrum.findtext('fMin')) == 16
    assert float(node_spectrum.findtext('fMax')) == 16000
    freq = _find(node_spectrum, 'freqTab')
    spectrum = [float(v) for v in freq.text.split()]
    assert len(spectrum) == 31
    return spectrum


def _read_wall_spectra(geonode):
    sp_types = ['raw_isolation', 'sabine_inner_absorption', 'sabine_outer_absorption']
    spectra = {}
    for index, mat in enumerate(geonode.findall('Paroi/MateriauConstruction')):
        if index == 0:
            spectrum = _parse_spectrum(mat.findall('Spectre')[0])
        else:
            spectrum = _parse_spectrum(mat.findall('Spectre')[1])
        spectra[sp_types[index]] = spectrum
    return spectra


SoundSource = namedtuple('SoundSource', ['name', 'vertices', 'spectrum'])


SoundReceiver = namedtuple('SoundReceiver', ['name', 'vertices', 'spectrum'])


Screen = namedtuple(
    'Screen',
    ['name', 'geotype', 'matrix', 'vertices', 'elevation', 'height', 'thickness']
)


Infra = namedtuple(
    'Infra',
    ['name', 'geotype', 'matrix', 'walls', 'elevation', 'walls_group_spectra']
)


LevelCurve = namedtuple(
    'LevelCurve',
    ['name', 'matrix', 'vertices', 'edges', 'height']
)


WaterBody = namedtuple(
    'WaterBody',
    ['name', 'matrix', 'vertices', 'faces', 'height']
)


Ground = namedtuple(
    'Ground',
    ['name', 'matrix', 'vertices', 'faces', 'color', 'resistivity']
)


class XMLImporter:

    def __init__(self, context, fpath=None):
        self.context = context
        self.root = self._load_file(fpath)
        self.site = self._get_sitenode()
        self.calculnode = self._get_current_calcul_node()
        self.src_name_by_indice = self._get_src_names_by_indices()
        self.rec_name_by_indice = self._get_rec_names_by_indices()

    def _load_file(self, fpath):
        if os.path.exists(fpath):
            with open(fpath, 'rb') as f:
                tree = etree.parse(f)
                root = tree.getroot()
                if root.tag != 'Tympan':
                    raise TympanXMLParseError(
                        'unexpected root element {}'.format(root)
                    )
            return root

    def _get_sitenode(self):
        if self.root is not None:
            sites = self.root.findall('Projet/SiteNode')
            if not sites:
                return None
            if len(sites) != 1:
                raise NotImplementedError('multiple sites not handled')
            return sites[0]

    def _get_current_calcul_node(self):
        if self.root is not None:
            current_calcul_id = self.root.findtext('Projet/currentCalculID')
            if current_calcul_id is None:
                return
            return self.root.find(f'Projet/Calcul[@id="{current_calcul_id}"]')

    def _get_src_names_by_indices(self):
        src_name_by_indice = {}
        if self.root is not None:
            inode = _find(self.root, 'Projet/SiteNode/Infrastructure')
            for snode in inode.findall('ListSource/GeometryNode/UserSourcePonctuelle'):
                src_name_by_indice[snode.get('id')] = snode.get('name')
            for bnode in inode.findall('ListBuilding/GeometryNode/Building'):
                src_name_by_indice[bnode.get('id')] = bnode.get('name')
            for mnode in inode.findall('ListMachine/GeometryNode/Machine'):
                src_name_by_indice[mnode.get('id')] = mnode.get('name')
        return src_name_by_indice

    def _get_rec_names_by_indices(self):
        rec_name_by_indice = {}
        if self.root is not None:
            for recnode in self.root.findall('Projet/PointsControl/PointControl'):
                rec_name_by_indice[recnode.get('id')] = recnode.get('name')
        return rec_name_by_indice

    def _read_level_curves(self):
        if self.site is None:
            return
        for geonode in self.site.findall('Topographie/ListCrbNiv/GeometryNode'):
            lcs_matrix = _transform_matrix(geonode)
            lcnode = _find(geonode, 'CourbeNiveau')
            height = float(_find(lcnode, 'altitude').text)
            vertices = [_coords(p) for p in lcnode.findall('Point')]
            edges = [(v1, v2) for v1, v2 in
                     zip(range(len(vertices)-1), range(1, len(vertices)))]
            yield LevelCurve(
                lcnode.get('name'), lcs_matrix, vertices, edges, height
            )

    def _read_water_bodies(self):
        if self.site is None:
            return
        for geonode in self.site.findall('Topographie/ListPlanEau/GeometryNode'):
            lcs_matrix = _transform_matrix(geonode)
            name = _find(geonode, 'PlanEau').get('name')
            lcnode = _find(geonode, 'PlanEau/CourbeNiveau')
            height = float(_find(lcnode, 'altitude').text)
            vertices = [_coords(p) for p in lcnode.findall('Point')]
            faces = [range(len(vertices))]
            yield WaterBody(name, lcs_matrix, vertices, faces, height)

    def _read_grounds(self):
        if self.site is None:
            return
        for geonode in self.site.findall('Topographie/ListTerrain/GeometryNode'):
            matrix = _transform_matrix(geonode)
            ground_node = _find(geonode, 'Terrain')
            name = ground_node.get('name')
            point_nodes = ground_node.findall('Point')
            if not point_nodes:
                continue
            vertices = [_coords(p) for p in point_nodes]
            faces = [range(len(vertices))]
            color_node = _find(ground_node, 'Color')
            color = (
                float(color_node.get('r')),
                float(color_node.get('g')),
                float(color_node.get('b')),
                1,
            )
            resistivity = float(ground_node.findtext('Sol/resistivite'))
            yield Ground(name, matrix, vertices, faces, color, resistivity)

    def _read_sources(self):
        if self.site is None:
            return
        for geonode in self.site.findall('Infrastructure/ListSource/GeometryNode'):
            matrix = _transform_matrix(geonode)
            source = _find(geonode, 'UserSourcePonctuelle')
            source_point = _find(source, 'Point')
            spectrum = source.find('UserSrcRegime/Spectre')
            if spectrum is not None:
                freqTab = _find(spectrum, 'freqTab')
                assert freqTab.get('form') == "0", (
                    'expecting {} spectrum to be scaled in '
                    'one-third octave bands'.format(freqTab))
                spectrum = _to_solver_spectrum(
                    [float(value) for value in freqTab.text.split()]
                )
            yield SoundSource(
                source.get('name'),
                matrix @ _coords(source_point),
                spectrum,
            )

    def _read_receivers(self):
        if self.root is None:
            return
        receivers_spectra = self._read_receivers_spectra()
        for receiver in self.root.findall('Projet/PointsControl/PointControl'):
            index = self.rec_name_by_indice[receiver.get('id')]
            spectrum = receivers_spectra.get(index, _to_solver_spectrum([-200.]*31))
            yield SoundReceiver(receiver.get('name'), _coords(receiver), spectrum)

    def _get_walls_groups_spectra(self, geonode):
        walls = {}
        floor = geonode.find('Etage')
        if floor is not None:
            default_spectra = _read_wall_spectra(floor)
            index = 1
            for wall in geonode.findall('Etage/ListMur/GeometryNode/Mur'):
                wall_name = wall.get('name')
                if wall.findtext('paroiLocked') == '0':
                    walls[wall_name] = {
                        'group_name': 'default',
                        'spectra': default_spectra
                    }
                else:
                    walls[wall_name] = {
                        'group_name': f'group{index}',
                        'spectra': _read_wall_spectra(wall)
                    }
                    index += 1
        return walls

    def _read_infrastructures(self, infra_type):
        if self.site is None:
            return
        inode = _find(self.site, 'Infrastructure')
        for geonode in inode.findall('List{}/GeometryNode'.format(infra_type)):
            infras_matrix = _transform_matrix(geonode)
            infras_elevation = float(_find(geonode, 'hauteur').text)
            for infra_node in geonode.findall(infra_type):
                for subgeonode in infra_node.findall('GeometryNode'):
                    infra_name = infra_node.get('name')
                    infra_matrix = _transform_matrix(subgeonode)
                    global_matrix = infras_matrix @ infra_matrix
                    infra_elevation = float(_find(subgeonode, 'hauteur').text)
                    elevation = infras_elevation + infra_elevation
                    for screen_node in subgeonode.findall('Ecran'):
                        name, vertices, height, thickness = _get_screen_infra(screen_node)
                        yield Screen(
                            name, 'screen', global_matrix, vertices, elevation,
                            height, thickness,
                        )
                    for box_node in subgeonode.findall('AcousticBox'):
                        walls = _get_cubic_infra(infra_node)
                        walls_group_spectra = self._get_walls_groups_spectra(subgeonode)
                        yield Infra(
                            infra_name, 'cubic', global_matrix, walls, elevation,
                            walls_group_spectra
                        )
                    for cylinder_node in subgeonode.findall('AcousticCylinder'):
                        walls = _get_cylindric_infra_walls(infra_node)
                        walls_group_spectra = self._get_walls_groups_spectra(subgeonode)
                        yield Infra(
                            infra_name, 'cylindric', global_matrix, walls, elevation,
                            walls_group_spectra
                        )
                    for floor_node in subgeonode.findall('Etage'):
                        floor_name = '/'.join([infra_name, floor_node.get('name')])
                        walls = _get_polygonal_infra_walls(subgeonode)
                        walls_group_spectra = self._get_walls_groups_spectra(subgeonode)
                        yield Infra(
                            floor_name, 'polygonal', global_matrix, walls, elevation,
                            walls_group_spectra
                        )

    def _read_sources_spectra(self):
        if self.calculnode is None:
            return {}
        spectra = {}
        for src_node in self.calculnode.findall('Resultat/ListLw/SourceLw'):
            xml_src_id = src_node.get('srcId')
            spectra[self.src_name_by_indice[xml_src_id]] = _to_solver_spectrum(
                _parse_spectrum(src_node.findall('Spectre')[0])
            )
        return spectra

    def _read_receivers_spectra(self):
        spectra = {}
        if self.calculnode is not None:
            for rec_node in self.calculnode.findall('ResuCtrlPnts/Recepteur'):
                xml_rec_id = rec_node.get('receptor_id')
                spectra[self.rec_name_by_indice[xml_rec_id]] = _to_solver_spectrum(
                    _parse_spectrum(rec_node.findall('Spectre')[0])
                )
        return spectra

    def _read_solver_parameters(self):
        cfg = ConfigParser()
        cfg.read_string(self.calculnode.findtext('solverParams'))
        return cfg

    def _get_src_indices_by_indexes(self):
        src_indices = {}
        if self.calculnode is not None:
            for src_node in self.calculnode.findall('Resultat/ListSources/Source'):
                src_indices[src_node.get('index')] = src_node.get('srcId')
        return src_indices

    def _get_rec_indices_by_indexes(self):
        rec_indices = {}
        if self.calculnode is not None:
            for rec_node in self.calculnode.findall('Resultat/ListRecepteurs/Recepteur'):
                rec_indices[rec_node.get('index')] = rec_node.get('recId')
        return rec_indices

    def _read_calcul_couples_spectra(self):
        spectra = {}
        src_id_by_index = self._get_src_indices_by_indexes()
        rec_id_by_index = self._get_rec_indices_by_indexes()
        for sp_node in self.calculnode.findall('Resultat/ListSpectres/SpectreInd'):
            src_name = self.src_name_by_indice[src_id_by_index[sp_node.get('indexSrc')]]
            rec_name = self.rec_name_by_indice[rec_id_by_index[sp_node.get('indexRec')]]
            spectra[(src_name, rec_name)] = _to_solver_spectrum(
                _parse_spectrum(sp_node.findall('Spectre')[0])
            )
        return spectra

    def _load_tympan_results(self):
        src_spectra = self._read_sources_spectra()
        rec_spectra = self._read_receivers_spectra()
        couples_spectra = self._read_calcul_couples_spectra()
        result = results.TympanResults(src_spectra, rec_spectra, couples_spectra)
        exporter = io_html_result.TympanResultExporter(result)
        result_file_path = os.environ['TYMPAN_RESULT_FILE']
        exporter.write_html_result(result_file_path)

    def _import_topography(self):
        for level_curve in self._read_level_curves():
            altimetry.make_level_curve(self.context, level_curve)
        for water_body in self._read_water_bodies():
            altimetry.make_water_body(self.context, water_body)
        for ground in self._read_grounds():
            altimetry.make_ground(self.context, ground)

    def _import_sources(self):
        for source in self._read_sources():
            o = element_types.make_acoustic_source(self.context, name=source.name)
            ops.transform.translate(value=source.vertices)
            spectrum_property = o.tympan_soundsource_properties[0].spectrum
            tu.initialise_spectrum(spectrum_property, source.spectrum.values)

    def _import_receivers(self):
        for receiver in self._read_receivers():
            o = element_types.make_acoustic_receiver(
                self.context,
                name=receiver.name
            )
            ops.transform.translate(value=receiver.vertices)
            spectrum_property = o.tympan_soundreceiver_properties[0].spectrum
            tu.initialise_spectrum(spectrum_property, receiver.spectrum.values)

    def _import_infrastructure(self):
        for machine in self._read_infrastructures('Machine'):
            if machine.geotype == 'cubic':
                infra.make_cubic_infra(self.context, machine, 'MACHINE')
            elif machine.geotype == 'cylindric':
                infra.make_cylindric_infra(self.context, machine, 'MACHINE')
            elif machine.geotype == 'polygonal':
                infra.make_polygonal_infra(self.context, machine, 'MACHINE')
        for building in self._read_infrastructures('Batiment'):
            if building.geotype == 'cubic':
                infra.make_cubic_infra(self.context, building, 'BUILDING')
            elif building.geotype == 'cylindric':
                infra.make_cylindric_infra(self.context, building, 'BUILDING')
            elif building.geotype == 'polygonal':
                infra.make_polygonal_infra(self.context, building, 'BUILDING')
            elif building.geotype == 'screen':
                infra.make_screen_infra(self.context, building)

    def _import_computations(self):
        # for now we only read the current calcul results
        if self.calculnode is not None:
            solver_params = self._read_solver_parameters()
            solver_properties = self.context.scene.solver_properties
            _load_solver_parameters(solver_properties, solver_params)
            if len(self.calculnode.findall('Resultat/ListSources/Source')) > 0:
                self._load_tympan_results()

    def import_file(self):
        self._import_topography()
        self._import_sources()
        self._import_receivers()
        self._import_infrastructure()
        self._import_computations()


def register():
    register_class(TympanXMLImport)
    TOPBAR_MT_file_import.append(menu_draw)


def unregister():
    unregister_class(TympanXMLImport)
    TOPBAR_MT_file_import.remove(menu_draw)
