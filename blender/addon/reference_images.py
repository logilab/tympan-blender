import math

import bpy
from mathutils import Vector
import bgl
from gpu_extras.batch import batch_for_shader
from bpy_extras.view3d_utils import (
    region_2d_to_location_3d,
)
from bpy.props import (
    BoolProperty,
    EnumProperty,
    FloatProperty,
    StringProperty,
)
from bpy.utils import (
    register_class,
    unregister_class,
)
from bpy.types import (
    Operator,
    SpaceView3D,
)
from .element_types import (
    is_reference_image,
    type_icon,
)

from . import tympan_utils as tu


def mouse_ray_get(context, event):
    co = event.mouse_region_x, event.mouse_region_y
    region = context.region
    r_data = context.space_data.region_3d

    location = region_2d_to_location_3d(region, r_data, co, Vector([0, 0, 0]))
    return location


def get_scale_line_drawer():
    import gpu
    line_shader = gpu.shader.from_builtin('3D_UNIFORM_COLOR')

    def draw_scale_line(operator):
        if not (operator._line_start and operator._line_end):
            return
        bgl.glEnable(bgl.GL_BLEND)
        bgl.glBlendFunc(bgl.GL_ONE_MINUS_DST_COLOR, bgl.GL_ONE_MINUS_SRC_COLOR)
        line_shader.bind()
        line_shader.uniform_float('color', (1, 1, 1, 1))
        bgl.glLineWidth(2)

        coords = [operator._line_start, operator._line_end]
        batch = batch_for_shader(line_shader, 'LINES', {"pos": coords})
        batch.draw(line_shader)
        bgl.glBlendFunc(bgl.GL_SRC_COLOR, bgl.GL_DST_COLOR)

        bgl.glLineWidth(1)
        bgl.glDisable(bgl.GL_BLEND)
        bgl.glEnable(bgl.GL_DEPTH_TEST)

    return draw_scale_line


class DrawLineOperatorMixin():
    """Drawing a line"""
    _line_start = None
    _line_end = None
    _line_start_region = None

    def invoke(self, context, event):
        bpy.ops.view3d.view_axis(type='TOP')
        space = context.area.spaces[0]
        self._shading_type = space.shading.type
        self._region_3d_perspective = space.region_3d.view_perspective
        self._show_region_header = space.show_region_header
        self._show_region_toolbar = space.show_region_toolbar
        self._show_region_ui = space.show_region_ui
        space.shading.type = 'WIREFRAME'
        space.region_3d.view_perspective = 'ORTHO'
        space.show_region_header = False
        space.show_region_toolbar = False
        space.show_region_ui = False
        context.window_manager.modal_handler_add(self)
        self._draw_handle = SpaceView3D.draw_handler_add(
            get_scale_line_drawer(), (self,), 'WINDOW', 'POST_VIEW')
        return {'RUNNING_MODAL'}

    def _finish(self, context):
        space = context.area.spaces[0]
        space.show_region_header = self._show_region_header
        space.show_region_toolbar = self._show_region_toolbar
        space.show_region_ui = self._show_region_ui
        space.shading.type = self._shading_type
        space.region_3d.view_perspective = self._region_3d_perspective
        SpaceView3D.draw_handler_remove(self._draw_handle, 'WINDOW')

    def _should_cancel(self, event):
        return event.type in {'ESC', 'RIGHTMOUSE'}

    def _should_prevent_default(self, event):
        """Prevent view3d rotation due to middle click and drag
        """
        return event.type == 'MIDDLEMOUSE' and not event.shift

    def _reset(self):
        self._line_start = None
        self._line_end = None
        self._line_start_region = None

    def _handle_wait_press(self, context, event):
        location = mouse_ray_get(context, event)
        if event.type == 'LEFTMOUSE':
            if event.value == 'PRESS':
                self._line_start_region = Vector([
                    event.mouse_region_x,
                    event.mouse_region_y
                ])
                self._line_start = location
        else:
            return {'PASS_THROUGH'}
        return {'RUNNING_MODAL'}

    def _released_too_fast(self, event):
        line_end_region = Vector([
            event.mouse_region_x,
            event.mouse_region_y
        ])
        vec_region = self._line_start_region - line_end_region
        return vec_region.length < 10

    def _handle_wait_release(self, context, event):
        location = mouse_ray_get(context, event)
        if event.type == 'MOUSEMOVE' and self._line_start:
            self._line_end = location
        elif event.type == 'LEFTMOUSE':
            if self._released_too_fast(event):
                self.report({'ERROR'},
                            'Draw a reference line or right click to cancel.')
                self._reset()
                return {'RUNNING_MODAL'}

            if event.value == 'RELEASE':
                self._line_end = location
                self._finish(context)
                return self.execute(context)
        else:
            return {'PASS_THROUGH'}
        return {'RUNNING_MODAL'}

    def _clamp_cursor(self, context, event):
        region = context.region
        mouse_x = event.mouse_region_x
        mouse_y = event.mouse_region_y
        in_x_bounds = 0 <= mouse_x <= region.width
        in_y_bounds = 0 <= mouse_y <= region.height
        if not (in_x_bounds and in_y_bounds):
            window = context.window
            left = region.x
            right = region.x + region.width
            bottom = region.y
            top = region.y + region.height
            window.cursor_warp(max(min(region.x + mouse_x, right), left),
                               max(min(region.y + mouse_y, top), bottom))

    def modal(self, context, event):
        context.window.cursor_set('CROSSHAIR')
        self._clamp_cursor(context, event)
        context.area.tag_redraw()
        if self._should_cancel(event):
            self._finish(context)
            return {'CANCELLED'}
        if self._should_prevent_default(event):
            return {'RUNNING_MODAL'}

        if self._line_start is None:
            return self._handle_wait_press(context, event)
        else:
            return self._handle_wait_release(context, event)

    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return obj and is_reference_image(obj)


class TympanDrawImageScaleOperator(Operator, DrawLineOperatorMixin):
    """Set the reference image's scale by drawing a line of known length"""
    bl_label = 'Draw image scale'
    bl_idname = tu.blty_idname('draw_image_scale')

    def execute(self, context):
        d = (self._line_start - self._line_end).length
        bpy.ops.blty.set_image_scale('INVOKE_AREA', measured_distance=d,
                                     lock_measured_distance=True)
        return {'FINISHED'}


class TympanDrawImageNorthOperator(Operator, DrawLineOperatorMixin):
    """Set the reference image's north by drawing a line known direction"""
    bl_label = 'Draw image north'
    bl_idname = tu.blty_idname('draw_image_north')

    def execute(self, context):
        obj = context.active_object
        direction = self._line_end - self._line_start
        new_north_angle = math.atan2(direction.y, direction.x)
        previous_north_angle = obj.rotation_euler.z + math.pi/2
        angle_delta = previous_north_angle - new_north_angle
        obj.rotation_euler[2] = angle_delta % (2 * math.pi)
        return {'FINISHED'}


class TympanSetScaleSizeOperator(Operator):
    """Set the reference image's scale by providing a length in the viewport
    and its corresponding true length"""
    bl_label = 'Set reference image scale'
    bl_idname = tu.blty_idname('set_image_scale')

    lock_measured_distance: BoolProperty(
        name='Lock Measured Distance',
        default=False,
    )
    measured_distance: FloatProperty(
        name='Measured Distance',
        description='Distance measured in the 3D view.',
        default=1,
        min=0,
    )
    true_distance: FloatProperty(
        name='True distance',
        description='Equivalent distance in reality',
        default=1,
        min=0,
    )

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        self.layout.use_property_split = True
        row = self.layout.row()
        if self.lock_measured_distance:
            row.enabled = False
        row.prop(self, 'measured_distance')

        row = self.layout.row()
        row.prop(self, 'true_distance')

    def _validate_props(self):
        if self.true_distance == 0:
            return f'Invalid true distance {self.true_distance}'
        if self.measured_distance == 0:
            return f'Invalid measured distance {self.measured_distance}'

    def execute(self, context):
        error = self._validate_props()
        if error:
            self.report({'ERROR'}, error)
            return {'CANCELLED'}
        obj = context.active_object
        ratio = self.true_distance / self.measured_distance
        obj.scale.xy *= ratio
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return obj and is_reference_image(obj)


class TympanAddReferenceImageOperator(Operator):
    """Import a reference image"""
    bl_label = 'Import a reference image'
    bl_idname = tu.blty_idname('import_reference_image')

    filepath: StringProperty()
    image_type: EnumProperty(
        items=(
            ("TOPOGRAPHIC_MAP", "Topographic Map", "Import a topographic map",
             type_icon('REFERENCE_IMAGE', "TOPOGRAPHIC_MAP"), 0),
            ("MASS_PLAN", "Mass Plan", "Impot a mass plan",
             type_icon('REFERENCE_IMAGE', 'MASS_PLAN'), 1)
        ),
        default="TOPOGRAPHIC_MAP"
    )

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):

        bpy.ops.object.empty_add(
            'INVOKE_REGION_WIN',
            type='IMAGE',
            location=[0, 0, 0],
        )

        try:
            image = bpy.data.images.load(self.filepath, check_existing=True)
        except RuntimeError as ex:
            self.report({'ERROR'}, str(ex))
            return {'CANCELLED'}

        obj = context.active_object
        obj.data = image
        obj.tympan_type = 'REFERENCE_IMAGE'
        addon_prefs = bpy.context.preferences.addons['blty'].preferences
        image_size = addon_prefs.image_size
        obj.tympan_subtype = self.image_type
        obj.scale.xy = (image_size, image_size)
        bpy.ops.blty.draw_image_scale('INVOKE_DEFAULT')
        return {'FINISHED'}


classes = (
    TympanDrawImageScaleOperator,
    TympanDrawImageNorthOperator,
    TympanSetScaleSizeOperator,
    TympanAddReferenceImageOperator,
)


def register():
    for cls in classes:
        register_class(cls)


def unregister():
    for cls in classes:
        unregister_class(cls)
