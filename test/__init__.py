from os import path
import pytest

HERE = path.abspath(path.dirname(__file__))


if __name__ == '__main__':
    # When called from `blty test` we get the full "blender" command line
    # arguments which are not usable for unittest.main(). So rebuild an empty
    # "argv".
    import sys
    argv = []
    if '--' in sys.argv:
        argv = sys.argv[sys.argv.index('--') + 1:]
    sys.exit(pytest.main(argv))
