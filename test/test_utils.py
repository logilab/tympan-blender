from numpy.testing import (
    assert_almost_equal
)

from blty.testutils import BlTYBaseTests
from blty.tympan_utils import (
    sum_db,
)


class SumDbTests(BlTYBaseTests):

    def test_two_times_50(self):
        spectra_to_sum = [[50], [50]]
        expected = [53]

        total = sum_db(spectra_to_sum)

        assert_almost_equal(total, expected, 2)

    def test_single_spectrum(self):
        initial_spectrum = [1, 2, 3]

        total = sum_db([initial_spectrum])

        assert_almost_equal(total, initial_spectrum, 2)

    def test_three_different_spectra(self):
        spectrum1 = [1, 15]
        spectrum2 = [10, 10]
        spectrum3 = [15, 1]

        total = sum_db([spectrum1, spectrum2, spectrum3])

        assert_almost_equal(total, [16.32, 16.32], 2)
