Lancement de simulations
========================

Le lancement de simulations (appels à Code Tympan), se fait via l'onglet
``Solve`` accessible depuis le panneau de gauche.

Les actions disponibles sont :

* calcul de l'altimétrie
* calcul du problème acoustique

Résultats
---------

Les résultats d'une simulation sont visibles pour chaque récepteur dans le
panneau "Object data" sous la forme d'un spectre de puissance :

.. figure:: images/blender-receiver-results.png

   Visualisation du spectre acoustique à un récepteur
