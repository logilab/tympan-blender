Références
----------

* Le plugin Archimesh_ : ajoute des éléments métiers (par ex. "Room", "Door",
  "Stairs") et d'autre action dans un onglet dédié (vertical à gauche)

* Le plugin GIS_ : gestion des fonds de cartes (Google Maps, OSM) et
  triangulation.

.. _Archimesh: https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Add_Mesh/Archimesh
.. _GIS: https://github.com/domlysz/BlenderGIS/
