Feuille de route de développement
=================================

Questions
---------

Que faire de l'emprise par défaut ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Actuellement, on crée une emprise (un carré entre de 200 de coté) pour
  définir l'emprise du site lors du calcul d'altimétrie. Cet objet est
  caché, il n'est pas visible dans la scène.
* On pourrait créer un objet dans la scène à l'ouverture.
* Les dimensions de l'emprise pourraient être une propriété du site. Le choix
  serait alors laissé à l'utilisateur d'utiliser l'emprise comme courbe de niveau.

Propositions
------------

Utilisation du moteur de maillage de Blender pour l'altimétrie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Blender dispose d'un moteur de maillage permettant *a priori* de remplacer
tout ou partie des fonctionnalités du module d'altimétrie de Code Tympan. Au
delà, il existe des extensions (par exemple, l'extension BlenderGIS_) qui sont
dédiées à la gestion de données topographiques.

Pour bien cerner le besoin, il conviendrait de considérer les différents
usages de l'interface de Code Tympan (création d'objets, import de données
cartographiques ou images satellite).

Participation des objets au calcul
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il pourrait être intéressant de cacher un objet sans pour autant
l'enlever du calcul. Une possibilité serait d'utiliser les calques
afin de classer les objets (participe au calcul/ne participe pas au
calcul).

Ajustement de l'altitude des infrastructures
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Après le calcul de l’emprise au sol d'une infrastructure dans le
calcul d’altimétrie, il serait possible de repositionner
l'infrastructure à la bonne hauteur.

Import/Export des projets au format sérialisé
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'import/export en format sérialisé (XML, JSON, YAML ...)
permettrait une meilleure gestion des projets:
* lecture du fichier par un humain
* modification manuelle d'un projet
* simplification de la gestion de cas test de validation

Fusion des entrepôts code_tympan et tympan_blender
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cette fusion permettrait la création d'une branche dans laquelle on
pourrait enlever l'ancienne interface au profit de la nouvelle. Cette
évolution ferait aussi gagner en temps de compilation.



.. _BlenderGIS: https://github.com/domlysz/BlenderGIS/
