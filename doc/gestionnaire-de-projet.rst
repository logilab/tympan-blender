Gestionnaire de projet
======================

L'ensemble des objets de la scène est disponible dans Blender via le panneau
``Project`` visible à gauche de l'interface. On peut notamment :

* sélectionner un ou plusieurs objet(s) pour édition

* sélectionner les objets participant au calcul

* filtrer les objets par sous-types

Un triangle est affiché à gauche du nom de l'élément dont les propriétés sont
affichées dans le panneau de droite.

.. figure:: images/blender-project-panel.png

   Gestionnaire de projet
