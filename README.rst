.. image:: https://ci.appveyor.com/api/projects/status/2tr36be7bcav7yi2?svg=true
   :target: https://ci.appveyor.com/project/logilab/tympan-blender
   :alt: Appveyor Build status

Starting Blender with Code Tympan preferences
=============================================

Running
=======

Run ``./blty start`` script from the command-line. Normally, Blender should start.

Optional arguments for Blender should be passed after a ``--``. For instance,
to start a Python console with Blender environment:

::

    ./blty start -- --background --python-console

Testing
=======

Run ``./blty test -- <arguments for unittest>``.

Environment setup
=================

Assuming Code Tympan in installed at ``$TYMPAN_INSTALL_DIR``, the following
environment variables need to be set to run a simulation through the Blender
interface.

::

    TYMPAN_SOLVERDIR=$TYMPAN_INSTALL_DIR/pluginsd
    CGAL_BINDINGS_PATH=$TYMPAN_INSTALL_DIR/cython_d/CGAL/
    PYTHONPATH=$TYMPAN_INSTALL_DIR/cython_d/
    LD_LIBRARY_PATH=$TYMPAN_INSTALL_DIR/lib
